<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Penginapans;
use App\Models\Status;
use Carbon\Carbon;

class IsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tanggalsekarang = Carbon::now();
        $tanggalan = $tanggalsekarang->copy()->addDays(30);

        // Ambil semua data 'Penginapans'
        $datapenginapans = Penginapans::all();

        foreach ($datapenginapans as $penginapan) {
            // Inisialisasi tanggal mulai dari tanggal saat ini
            $tanggal = $tanggalsekarang->copy();

            // Membuat 30 tanggal untuk setiap 'id_penginapan'
            for ($i = 1; $i <= 30; $i++) {
                Status::create([
                    'id_penginapan' => $penginapan->id,
                    'tanggal' => $tanggal->toDateString(),
                    'status' => 'Kosong',
                ]);

                // Tambah 1 hari untuk iterasi berikutnya
                $tanggal->addDay();
            }
        }
    }
}
