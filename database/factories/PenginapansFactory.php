<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Penginapans>
 */
class PenginapansFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nama_penginapan' => $this->fake()->nama_penginapan,
            'alamat' => $this->fake()->alamat,
            'deskripsi' => $this->fake()->deskripsi,
            'gambar' => $this->fake()->imageUrl(),
            'tipe' => $this->faker->randomElement(['Hotel', 'Resort', 'Villa']),
            'harga' => $this->fake()->harga,
        ];
    }
}
