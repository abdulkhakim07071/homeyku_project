<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        

        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal');
            $table->unsignedBigInteger('id_penginapan');
            $table->unsignedBigInteger('id_pelanggan');
            $table->date('check_in');
            $table->date('check_out');
            $table->unsignedBigInteger('id_pembayaran');
            $table->foreign('id_penginapan')->references('id')->on('penginapans');
            $table->foreign('id_pelanggan')->references('id')->on('customers');
            $table->foreign('id_pembayaran')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bookings');
    }
};
