<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fasilitas', function (Blueprint $table) {
            $table->id();
            $table->string('parkiran');
            $table->string('tipe_bed');
            $table->integer('kamar_mandi');
            $table->integer('jumlah_kamar');
            $table->boolean('ac')->default(false);
            $table->boolean('wifi')->default(false);
            $table->boolean('kolam_renang')->default(false);
            $table->boolean('air_hangat')->default(false);
            $table->boolean('breakfast')->default(false);
            $table->index('id');
            $table->timestamps();
        });

        Schema::create('penginapans', function (Blueprint $table) {
            $table->id();
            $table->string('nama_penginapan');
            $table->string('alamat');
            $table->text('deskripsi');
            $table->json('image');
            $table->string('tipe');
            $table->float('harga');
            $table->unsignedBigInteger('id_admin');
            $table->unsignedBigInteger('id_fasilitas');
            $table->timestamps();
            $table->foreign('id_admin')->references('id')->on('users');
            $table->foreign('id_fasilitas')->references('id')->on('fasilitas');
        });
        
        



    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

        Schema::dropIfExists('penginapans');
        Schema::dropIfExists('fasilitas');
    }
};
