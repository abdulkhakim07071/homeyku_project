<?php
use App\Http\Controllers\BookingController;
use App\Models\User;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\PenginapansController;
use Illuminate\Tests\Integration\Routing\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Route::get('/search', function () {
//     return view('search.search');
// });



// Route::get('/lihatdetail', function () {
//     return view('detail.detailvila');
// });

// Route::get('/detailkamar', function () {
//     return view('detail.detailkamar');
// });

// Route::get('/detailfasilitas', function () {
//     return view('detail.detailfasilitas');
// });


Route::get('/bookingvilla', [BookingController::class, 'booking'])->name('bookingvilla');
Route::POST('/review', [BookingController::class, 'review'])->name('reviewbooking');
Route::get('/pembayaran', [BookingController::class, 'payment'])->name('pembayaran');

Route::get('/payment', function () {
    return view('booking.pembayaran');
});
Route::post('/booking', function () {
    return view('booking.review');
});


Route::get('/detailRating', function () {
    return view('detail.detailRating');
});

Route::get('/detail/{id}', [PenginapansController::class, 'detail'])->name('detail');


Route::get('/search', [SearchController::class, 'index'])->name('search');
Route::get('/search/ajax', [SearchController::class, 'search'])->name('searchajax');
//Route::get('/penginapan/{id}', [SearchController::class, 'detail'])->name('detail');


Route::get('/login',[LoginController::class, 'index'])->middleware('guest');
Route::post('/login',[LoginController::class, 'authenticate'])->name('login');
Route::post('/logout',[LoginController::class, 'logout']);
Route::post('/logoutadmin',[LoginController::class, 'logoutadmin']);


Route::get('/register',[RegisterController::class, 'index']);
Route::post('/register',[RegisterController::class, 'store']);


Route::get('/adminregister',[RegisterController::class, 'admin']);
Route::post('/adminregister',[RegisterController::class, 'store_admin']);

Route::get('/adminlogin',[LoginController::class, 'admin'])->name('login')->middleware('guest');
Route::post('/adminlogin',[LoginController::class, 'authenticateadmin'])->name('loginadmin');


Route::get('/dashboard',[DashboardController::class, 'index'])->name('dashboard');

Route::get('/dashboardadmin',[AdminController::class, 'index'])->name('dashboardadmin');
Route::get('/create',[PenginapansController::class, 'index']);
Route::post('/create',[PenginapansController::class, 'store']);



Route::get('penginapan/{id}/edit', [PenginapansController::class, 'edit'])->name('edit');
Route::put('penginapan/{id}', [PenginapansController::class, 'update'])->name('update');

Route::post('penginapan/{id}/delete',[PenginapansController::class, 'destroy'])->name('delete');

Route::get('booking',[BookingController::class, 'index'])->name('booking');
    


