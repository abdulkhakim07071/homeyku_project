<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'nohp', 'email'];



    public function booking()
    {
        return $this->hasOne(Booking::class, 'id_pelanggan');
    }
}
