<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;
    public $timestamps = false;
protected $fillable = [
            'ratings',
            'id_penginapan'
        ];

        public function rating()
        {
            return $this->belongsTo(Penginapans::class, 'id_penginapan');
        }
    public function averageRating()
{
    return $this->ratings()->avg('rating');
}
}
