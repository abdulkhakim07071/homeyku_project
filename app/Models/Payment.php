<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['metode_pembayaran', 'tanggal_pembayaran', 'jumlah_pembayaran', 'status'];
    public $timestamps = false;
    public function bookings()
    {
        return $this->hasOne(Booking::class, 'id_pembayaran');
    }
}
