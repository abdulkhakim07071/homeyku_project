<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fasilitas extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'jumlah_kamar',
        'parkiran',
        'tipe_bed',
        'ac',
        'wifi',
        'kolam_renang',
        'air_hangat',
        'breakfast',
        'kamar_mandi'
    ];
    protected $casts = [
        'ac' => 'boolean',
        'wifi' => 'boolean',
        'kolam_renang' => 'boolean',
        'air_panas' => 'boolean',
        'breakfast' => 'boolean',
    ];
    
    public function penginapan()
    {
        return $this->hasOne(Penginapans::class, 'id_fasilitas');
    }
    
}
