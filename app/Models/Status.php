<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status'; // Nama tabel yang akan digunakan oleh model ini

    protected $fillable = [
        'id_penginapan', // Kolom-kolom yang bisa diisi secara massal (Mass Assignment)
        'tanggal',
        'status',
    ];

    // Jika model menggunakan timestamp (created_at dan updated_at), gunakan properti berikut:
    public $timestamps = false;

    // Jika model menggunakan soft deletes (deleted_at), gunakan properti berikut:
    // protected $dates = ['deleted_at'];

    // Definisikan relasi antara model "Status" dengan model "Penginapans" (contoh relasi One-to-Many)
    public function penginapan()
    {
        return $this->belongsTo(Penginapans::class, 'id_penginapan', 'id');
    }
    public static function IsiStatus()
    {
        // Tambahkan logika di sini untuk mengisi tanggal terdaftar berdasarkan reservasi yang ada
        // Misalnya, Anda dapat menggunakan data dari tabel reservasi untuk mengisi tabel "tanggal_terdaftar"
        // Pastikan Anda memeriksa dan validasi data dengan baik sebelum mengisinya ke tabel "tanggal_terdaftar"
    }
}
