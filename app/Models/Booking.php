<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['tanggal', 'id_penginapan', 'id_pelanggan', 'check_in', 'check_out', 'id_pembayaran'];


    public function penginapans()
    {
        return $this->belongsTo(Penginapans::class, 'id_penginapan');
    }

    public function payments()
    {
        return $this->belongsTo(Payment::class, 'id_pembayaran');
    }
    public function Customers()
    {
        return $this->belongsTo(User::class, 'id_pelanggan');
    }
}
