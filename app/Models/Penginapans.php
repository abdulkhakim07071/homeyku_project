<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penginapans extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_penginapan',
        'alamat',
        'deskripsi',
        'tipe',
        'image',
        'harga',
        'id_fasilitas',
        'id_admin',
    ];
    protected $table = 'penginapans';
    protected $primaryKey = 'id';


    protected $foreignKey = 'id_fasilitas';

    public function fasilitas()
    {
        return $this->belongsTo(Fasilitas::class, 'id_fasilitas');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'id_admin');
    }
    public function bookings()
    {
        return $this->hasOne(Booking::class, 'id_penginapan');
    }
    public function status()
    {
        return $this->hasOne(Status::class, 'id_penginapan');
    }
    public function ratings()
    {
        return $this->hasOne(Rating::class, 'id_penginapan');
    }
    public function averageRating()
    {
        if ($this->rating) {
            return $this->rating()->avg('rating');
                } else {
            return 0;
        }
    }


}
