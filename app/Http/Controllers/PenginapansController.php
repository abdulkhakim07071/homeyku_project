<?php

namespace App\Http\Controllers;

use App\Models\Fasilitas;
use App\Models\Penginapans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StorePenginapansRequest;
use App\Http\Requests\UpdatePenginapansRequest;

class PenginapansController extends Controller
{
    /**
     * Display a listing of the resource.
     */
        public function index(){
            return view('input.create', [
            'title' => 'Input Data Penginapan',
            'active'=> 'Input Data Penginapan'
        ]);
    }
    public function fasilitasindex(){
        return view('input.fasilitas', [
        'title' => 'Input Data Fasilitas',
        'active'=> 'Input Data Penginapan'
    ]);
}
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){

        $request->validate([
            'nama_penginapan' => 'required|unique:penginapans|max:255',
            'alamat' => 'required|max:255',
            'deskripsi' => 'required|max:255',
            'images' => 'required|max:20480',
            'tipe' => 'required',
            'harga' => 'required',
            'jumlah_kamar' => 'required',
            'kamar_mandi'=> 'required',
            'parkiran' => 'required',
            'tipe_bed' => 'required',
            'id_admin' => 'required|exists:users,id'
        ]);
        
        if ($request->hasFile('images')) {
            $gambar = []; // Membuat array untuk menyimpan nama-nama file gambar
            
            foreach ($request->file('images') as $image) {
                // Mendapatkan nama file gambar dan menyimpannya ke dalam array
                $gambar[] = $image->getClientOriginalName();
                
                // Pindahkan file gambar ke lokasi yang diinginkan
                $image->move('img/data/', $image->getClientOriginalName());
            }
        }
        $ac = $request->has('ac') ? true : false;
        $wifi = $request->has('wifi') ? true : false;
        $kolam_renang = $request->has('kolam_renang') ? true : false;
        $breakfast = $request->has('breakfast') ? true : false;
        $airhangat = $request->has('air_hangat') ? true : false;
        
        
        
        $fasilitas = Fasilitas::create([
            'jumlah_kamar' => $request->jumlah_kamar,
            'parkiran' => $request->parkiran,
            'tipe_bed' => $request->tipe_bed,
            'ac' => $ac,
            'wifi' => $wifi,
            'kolam_renang' => $kolam_renang,
            'breakfast' => $breakfast,
            'kamar_mandi' => $request->kamar_mandi,
            'air_hangat' => $airhangat,
        ]);
        
        // Buat objek model untuk tabel penginapans
        $penginapan = new Penginapans([
            'nama_penginapan' => $request->nama_penginapan,
            'alamat' => $request->alamat,
                'image' => json_encode($gambar),
                'deskripsi' => $request->deskripsi,
                'tipe' => $request->tipe,
                'harga' => $request->harga,
                'id_fasilitas' => $fasilitas->id,
                'id_admin' => $request->id_admin
                
            ]);
            

            $penginapan->fasilitas()->associate($fasilitas);
            $penginapan->save();
            
            return redirect('/dashboardadmin')->with('success', 'Input data Penginapan succesfull!!!');
            
        
    }

    public function edit($id)
    {
        $penginapan = Penginapans::findOrFail($id);
        //$fasilitas = Fasilitas::where('id', 'id_fasilitas')->get();
        $fasilitas = Fasilitas::findOrFail($penginapan->id_fasilitas);

        return view('input.edit', compact('penginapan', 'fasilitas'),[
            'title' => 'Edit Data Penginapan',
            'active'=> 'Edit Data Penginapan'
        ]);
    }


public function update(Request $request, $id)
{

    $penginapan = Penginapans::findOrFail($id);
    $fasilitas = Fasilitas::where('id', $penginapan->id_fasilitas)->first();

    // Validasi data penginapan yang diperbarui
    $request->validate([
        'nama_penginapan' => 'required',
        'alamat' => 'required',
        'harga' => 'required',
        'jumlah_kamar' => 'required',
        'parkiran' => 'required',
        'tipe_bed' => 'required',
    ]);
 

    // Update data penginapan
    $penginapan->update($request->only(['nama_penginapan', 'alamat', 'harga', 'images', 'tipe']));

    // Update data fasilitas
    $fasilitas->jumlah_kamar = $request->input('jumlah_kamar');
    $fasilitas->parkiran = $request->input('parkiran');
    $fasilitas->tipe_bed = $request->input('tipe_bed');
    $fasilitas->ac = $request->has('ac');
    $fasilitas->wifi = $request->has('wifi');
    $fasilitas->breakfast = $request->has('breakfast');
    $fasilitas->kolam_renang = $request->has('kolam_renang');
    $fasilitas->air_hangat = $request->has('air_hangat');
    $fasilitas->save();


    return redirect('/dashboardadmin')->with('success', 'Data penginapan berhasil diperbarui');

}




    public function destroy($id)
    {
        try {
            $penginapan = Penginapans::findOrFail($id);
            $fasilitas = Fasilitas::where('id', $penginapan->id_fasilitas)->first();

            // Hapus data penginapan
            $penginapan->delete();

            // Hapus data fasilitas jika ditemukan
            if ($fasilitas) {
                $fasilitas->delete();
            }

            return redirect('/dashboardadmin')->with('success', 'Data penginapan berhasil dihapus');
        } catch (\Exception $e) {
            $error = 'Terjadi kesalahan saat menghapus data penginapan';
            return redirect()->back()->withErrors($error);
        }
    }

    /**
     * Display the specified resource.
     */
    public function detail($id)
    {
        $query = Penginapans::with(['bookings', 'ratings', 'fasilitas', 'status']);
        $penginapan = $query->find($id);
        return view('detail.detailkamar', compact('penginapan'),[
            'title' => 'Detail Penginapan',
            'active'=> 'Detail Penginapan'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
}
