<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    //
    public function index(){
        return view('register.index', [
        'title' => 'register',
        'active'=> 'register'
    ]);
    }
    public function store(Request $request){
        $validatedData = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|unique:users|max:255',
                'password' => 'required|min:8',
                'role' => 'required'
            ]);

            $validatedData['password'] = bcrypt($validatedData['password']);
        User::create($validatedData);

        return redirect('/login')->with('success', 'Registration succesfull! Please login');
    }

    public function admin(){
        return view('register.admin', [
        'title' => 'register Admin',
        'active'=> 'register Admin'
    ]);
    }
    public function store_admin(Request $request){
        $validated = $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|unique:users|max:255',
                'password' => 'required|min:8',
                'role' => 'required'
            ]);

            $validated['password'] = bcrypt($validated['password']);
        User::create($validated);

        return redirect('/adminlogin')->with('success', 'Registration succesfull! Please login');
    }
}
