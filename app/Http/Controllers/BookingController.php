<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Booking;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\Fasilitas;
use App\Models\Penginapans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorebookingRequest;
use App\Http\Requests\UpdatebookingRequest;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $sortBy = $request->input('sort_by', 'asc');
        $id_admin = auth()->user()->id ;
        $booking = DB::table('bookings')
        ->leftjoin('users', 'users.id', '=', 'bookings.id_pelanggan')
        ->leftjoin('penginapans', 'penginapans.id', '=', 'bookings.id_penginapan')
        ->leftjoin('payments', 'payments.id', '=', 'bookings.id_pembayaran')
        ->where('id_admin', $id_admin)
        ->orderBy('tanggal', $sortBy)
        ->get();
  
        return view('dashboard.booking',[
            'booking'=> $booking,
            'title' => 'Data Booking',
            'active'=> 'Data Booking',
            'sort_by' => $sortBy
        ]);
    }
    public function booking(Request $request)
    {
        $paymentMethod = $request->input('paymentMethod');

        $user = auth()->user()->id ;
        $idKamar = $request->input('id');
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $pelanggan = DB::table('users')
        -> where('id',$user)
        ->first();

        $booking = Penginapans::with(['bookings', 'ratings', 'fasilitas', 'status'])
        ->where('id' ,$idKamar)
        ->first();
  
  
        return view('booking.bookingvilla',[
            'booking'=> $booking,
            'title' => 'Data Booking',
            'pelanggan'=> $pelanggan,
            'checkin' => $checkin,
            'checkout' => $checkout,
        ]);
    }
    public function payments(Request $request)
    {
        // $user =  ;
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $idKamar = $request->input('id');
        $name = $request->input('baru');
        $nohp = $request->input('nohp');
        $email = $request->input('email');

        Customer::create([
            'name' => $name,
            'nohp' => $nohp,
            'email' => $email,
        ]);
        
        $booking = Penginapans::with(['bookings', 'ratings', 'users', 'fasilitas', 'status'])
        ->where('id' ,$idKamar)
        ->first();
  
  
        return view('booking.pembayaran',[
            'booking'=> $booking,
            'title' => 'Review Booking',
            'checkin' => $checkin,
            'checkout' => $checkout,
        ]);
    }
    public function review(Request $request)
    {
        // $user =  ;
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $idKamar = $request->input('id');
        $name = $request->input('baru');
        $nohp = $request->input('nohp');
        $email = $request->input('email');
        $paymentMethod = $request->input('paymentMethod');
        $hari_ini = $request->input('hari');
        $bayar = $request->input('total');
        if($paymentMethod == 'transfer'){
            $status = 'Lunas';
        }else{
            $status = 'Belum Lunas';
        }

        $payment = Payment::create([
            'metode_pembayaran' => $paymentMethod,
            'tanggal_pembayaran' => $hari_ini,
            'jumlah_pembayaran' => $bayar,
            'status' => $status,

        ]);


        $customer = Customer::create([
            'name' => $name,
            'nohp' => $nohp,
            'email' => $email,
        ]);
        $bookings = new Booking([
            'tanggal' => $hari_ini,
            'check_in' => $checkin,
            'check_out' => $checkout,
            'id_penginapan' => $idKamar,
            'id_pelanggan' => $customer->id,
            'id_pembayaran'=> $payment->id,
        ]);
        $bookings->Payments()->associate($payment);
        $bookings->Customers()->associate($customer);
            $bookings->save();
        $booking = Penginapans::with(['bookings', 'ratings', 'users', 'fasilitas', 'status'])
        ->where('id' ,$idKamar)
        ->first();
  
  
        return view('booking.review',[
            'booking'=> $booking,
            'title' => 'Review Booking',
            'checkin' => $checkin,
            'checkout' => $checkout,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function showForm()
    {
        return view('booking.form');
    }

    public function showSuccess()
    {
        return view('booking.success');
    }

    public function submitForm(Request $request)
    {
        // Lakukan validasi data yang diterima dari form
        $this->validate($request, [
            'nama' => 'required',
            'tanggal_check_in' => 'required|date',
            'tanggal_check_out' => 'required|date|after:tanggal_check_in',
            'jumlah_orang' => 'required|integer|min:1',
            // tambahkan validasi lain sesuai kebutuhan
        ]);

        // Proses penyimpanan data booking ke database atau sistem lainnya

        // Tampilkan halaman sukses
        return view('booking.success');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorebookingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatebookingRequest $request, booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(booking $booking)
    {
        //
    }
}
