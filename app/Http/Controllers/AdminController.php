<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Penginapans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $id_admin = auth()->user()->id ;// Berikan nilai id_admin yang sesuai. Anda dapat mendapatkannya dari sesi atau pengguna saat ini.
 
        $penginapans = \App\Models\Penginapans::where('id_admin', $id_admin)->get();

            return view('dashboard.Admin',compact('penginapans') ,[
            'title' => 'Dashboard Admin',
            'active'=> 'Dashboard Admin'
            ]);
    }

   

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAdminRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
}
