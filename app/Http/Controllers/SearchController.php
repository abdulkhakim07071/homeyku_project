<?php

namespace App\Http\Controllers;
use App\Models\Booking;
use App\Models\Penginapans;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        // $penginapan = Penginapans::join('fasilitas', 'fasilitas.id', '=', 'penginapans.id_fasilitas')
        //     ->leftjoin('ratings', 'ratings.id_penginapan', '=', 'penginapans.id')
        //     // ->where('alamat', 'LIKE', "%$keyword%")
        //     ->find();
        $query = Penginapans::with(['bookings', 'ratings', 'fasilitas', 'status']);
        $penginapan = $query->get();

            


        return view('search.search', [
            'title'=>'search',
            'penginapan'=> $penginapan,
            // 'sort_by' => $sortBy
        ]);
    }

    public function search(Request $request)
    {
        // Dapatkan input dari form pencarian
        $location = $request->input('location');
        $checkin = $request->input('checkin');
        $checkout = $request->input('checkout');
        $jenis = $request->input('tipe');

        // Query penginapan sesuai dengan parameter pencarian
        $query = Penginapans::with(['bookings', 'ratings', 'fasilitas', 'status']);

        if ($location) {
            $query->where('alamat', 'like', '%' . $location . '%');
        }

        if ($checkin && $checkout) {
            $query->whereHas('status', function ($q) use ($checkin, $checkout) {
                $q->whereBetween('tanggal', [$checkin, $checkout])
                    ->where('status', 'kosong');
            });
        }
        if ($jenis) {
            $query->where('tipe', $jenis);
        }
        $penginapan = $query->get();

 
        if ($request->ajax()) {
            return response()->json([
                'penginapan' => $penginapan,
            ]);
        }

        // Jika request dari HTTP biasa, tampilkan hasil pencarian dalam view 'search.search'
        return view('search.search')->with('penginapan', $penginapan );
    }
        }
    
    

