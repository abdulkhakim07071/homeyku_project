<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function index(){
        return view('login.index', [
        'title' => 'Login',
        'active'=> 'login'
    ]);
    }

    public function authenticate(Request $request){
        $email      = $request->input('email');
        $password   = $request->input('password');
        $role       = $request->input('role');

        if(Auth::guard('web')->attempt(['email' => $email, 'password' => $password, 'role' => $role])) {
            return response()->json([
                'success' => true
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Login Gagal!'
            ], 401);
        }
 
    }
    public function logout(Request $request)
    {
        Auth::logout();
     
        $request->session()->invalidate();
     
        $request->session()->regenerateToken();
     
        return redirect('/login');
    }

    public function admin(){
        return view('login.admin', [
        'title' => 'Login Admin',
        'active'=> 'login Admin'
    ]);
    }
    public function authenticateadmin(Request $request){
        $email      = $request->input('email');
        $password   = $request->input('password');
        $role       = $request->input('role');

        if(Auth::guard('web')->attempt(['email' => $email, 'password' => $password, 'role' => $role])) {
            return response()->json([
                'success' => true
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Login Gagal!'
            ], 401);
        }

    }

    public function logoutadmin(Request $request)
    {
        Auth::logout();
     
        $request->session()->invalidate();
     
        $request->session()->regenerateToken();
     
        return redirect('/adminlogin');
    }


}
