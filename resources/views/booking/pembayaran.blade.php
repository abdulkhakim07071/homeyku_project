@extends('layouts.main')
@section('container')
<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="row text-center">
                <h5>METODE PEMBAYARAN</h5>
            </div>
            <div class="row ">

            <h6>PILIH METODE PEMBAYARAN</h6>
            </div>

    <div class="form-check row b1">
        <div class="col-4">
            <input class="form-check-input" type="radio" value="cash" name="paymentMethod" id="cash" data-toggle="collapse" data-target="#transferCollapse" aria-expanded="false" aria-controls="transferCollapse">
        </div>
        <div class="col-8 text-center">
            <label class="form-check-label" for="cash">
                Cash On location
            </label>
        </div>
    </div>
    <div class="form-check row b1">
        <div class="col-4">
            <input class="form-check-input" type="radio" value="transfer" name="paymentMethod" id="transfer" data-toggle="collapse" data-target="#transferCollapse" aria-expanded="true" aria-controls="transferCollapse">
        </div>
        <div class="col-8 text-center">
            <label class="form-check-label" for="transfer">
                Transfer
            </label>
        </div>
    </div>
   

    <div><h5>Rincian Harga</h5></div>
<div class="row margin:20px;">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <div class="card" style="width: 135%;">
            <div class="card-body" >
            <div class="row">
    <div class="col-8" >Total biaya</div>
    <div class="col-4"><h5>Rp. total,00</h5></div>
    </div>
                <hr>
                <div class="row g-0 text-left">
                    <div class="col-sm-6 col-md-8">jumlah_hari malam</div>
                    <div class="col-6 col-md-4">Rp  $harga ,00</div>
                    <div class="col-sm-6 col-md-8">Pajak</div>
                    <div class="col-6 col-md-4">Rp pajak,00</div>
                    <div class="col-sm-6 col-md-8">Platform fee</div>
                    <div class="col-6 col-md-4" style="color: #57C5B6;">Rp total ,00</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <button class="btn btn-success d-block mx-auto" type="submit" style="background-color: #57C5B6; border-radius: 5px;">Lanjutkan</button><br>

    

            </div>

            <br>
            <br>
            <br>
            <br>
        <div class="col-md-4" style="border-radius: 10px;" >
        <div class="card-header " style="background-color: #57C5B6; " ><h3 class="text-center">Arunika Villa</h3></div>
        <div class="mb-3 border p-3 border-radius: 10px; border: 0.50px black solid ">
            
            
            <hr>
            <div class="row">
                <div class="col">
                    Check-in 
                </div>
                <div class="col">
                    Check-out
                </div>
                <div class="col">
                    Type 
                </div>
        </div>
        <div class="row">
            <div class="col">
                <p>
            checkin           
        </p>
    </div>
    <div class="col">
            checkout 
        </div>
        <div class="col">
            Villa
        </div>
    </div>
    

<!-- Tambahkan input lainnya sesuai kebutuhan -->
</div>
        </div>
</div>
    </div>

<!-- Bootstrap JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" type="text/javascript"></script>

@endsection