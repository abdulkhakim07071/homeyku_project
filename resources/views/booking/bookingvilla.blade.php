@extends('layouts.main')
     
@section('container')
    <!-- <h2>Form Booking Villa</h2> -->
    
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @php
    $checkinTimestamp = strtotime($checkin);
    $checkoutTimestamp = strtotime($checkout);
    
    // Hitung selisih hari antara checkin dan checkout
    $jumlah_hari = ($checkoutTimestamp - $checkinTimestamp) / (60 * 60 * 24);
    
    $harga = $booking->harga * $jumlah_hari;
    $pajak = $harga * 10 /100;
    $fee = $harga * 5 /100;
    $total = $harga + $fee + $pajak;
    @endphp

    <h5 class="border text-left" style="padding: 10px; margin: 10px; display: inline-block; border-radius: 10px;">Pemesanan</h5>

    <form style="border-radius: 10px;" action="{{route('reviewbooking')}}" method="POST" enctype="multipart/form-data">
    @csrf
<div class="container">



    <div class="row">
        <div class="col-md-6">

                <br>
                <div class="mb-3 border p-3" style="border-radius: 10px;">
                <label for="name" class="form-label">Nama</label>
                <input type="text" class="form-control" id="name" name="baru" value="{{ $pelanggan->name }}" placeholder="Masukkan Nama Sesuai KTP" required>  

                <label for="phone" class="form-label">No. Handphone</label>
                <div class="input-group flex-nowrap">
                    <span class="input-group-text" id="addon-wrapping">
                    <select  name="country_code">
                        <option value="+62">+62</option>
                        <option value="+1t">+1</option>
                        <option value="44">+44</option>
                        <option value="+62">+62</option>
                        <!-- Tambahkan opsi lainnya sesuai kebutuhan -->
                    </select>
                </span>
                <input type="text" class="form-control" placeholder="" aria-label="phone" aria-describedby="addon-wrapping" name="nohp" id="nohp" required>
            </div>
            
            <label for="email" class="form-label">Alamat Email Anda</label>
            <input type="text" class="form-control" id="email" value="{{ $pelanggan->email }}" name="email" required>
            <input type="hidden" value="{{ $booking->id }}" id="id" name="id">
            <input type="hidden" value="{{ $checkin }}" id="checkin" name="checkin">
            <input type="hidden" value="{{ $checkout }}" id="checkout" name="checkout">
            <input type="hidden" value="{{ now()->format('Y-m-d') }}" id="hari" name="hari">
            <input type="hidden" value="{{ $total }}" id="total" name="total">
                </div>
                <br><br>
                <div class="row ">

<h6>PILIH METODE PEMBAYARAN</h6>
</div>

<div class="form-check row b1">
<div class="col-4">
<input class="form-check-input" type="radio" name="paymentMethod" id="cash" value="cash" data-toggle="collapse" data-target="#transferCollapse" aria-expanded="false" aria-controls="transferCollapse">
</div>
<div class="col-8 text-center">
<label class="form-check-label" for="cash">
    Cash On location
</label>
</div>
</div>
<div class="form-check row b1">
<div class="col-4">
<input class="form-check-input" type="radio" name="paymentMethod" id="transfer" value="transfer" data-toggle="collapse" data-target="#transferCollapse" aria-expanded="true" aria-controls="transferCollapse" checked>
</div>
<div class="col-8 text-center">
<label class="form-check-label" for="transfer">
    Transfer
</label>
</div>
</div>
                
                
                <div class="mb-3">
                    <label for="label1" class="form-label1"><h3>Permintaan Khusus</h3></label>
                    <p>*Punya permintaan khusus? Ajukan permintaan Anda dan properti akan berusaha memenuhinya. 
                        (Permintaan khusus tidak dijamin dan dapat dikenakan biaya)
                    </p>
                    <textarea class="form-control" id="text1" rows="3" style="border-radius: 10px;"></textarea>
                </div><br><br>
                
                <div><h5>Kebijakan Pembatalan</h5></div>
                
                <div class="mb-3" style="background: #57C5B6; box-shadow: 3px 3px 3px 3px rgba(0, 0, 0, 0.25); border-radius: 10px">
                    
                <div class="p-2 mb-3 text-white">Pembatalan Gratis Kebijakan Pembatalan: Pembatalan gratis sebelum tanggal 19-Mei-2023 13:00. 
                    Jika pesanan dibatal atau diubah setelah 19-Mei-2023 13:01, biaya pembatalan akan dikenakan</div>
                </div>


    <div><h5>Rincian Harga</h5></div>
    <div class="mb-3 border p-3" style="border-radius: 10px;">
        <div class="row">
    <div class="col-8" >Total biaya</div>
    <div class="col-4"><h5>Rp. {{$total}},00</h5></div>
    </div>
    <hr>
    <div class="row g-0 text-left">
        <div class="col-sm-6 col-md-8">({{$jumlah_hari}}x) Villa ({{$jumlah_hari}} malam)</div>
        <div class="col-6 col-md-4">Rp {{ $harga }},00</div>
        <div class="col-sm-6 col-md-8">Pajak</div>
        <div class="col-6 col-md-4">Rp {{$pajak}},00</div>
        <div class="col-sm-6 col-md-8">Platform fee</div>
        <div class="col-6 col-md-4" style="color: #57C5B6;">Rp {{ $total }},00</div>
    </div>
</div>
<br>


</div>
<div class="col-md-6" style="border-radius: 10px;" >
        <div class="card-header " style="background-color: #57C5B6; " ><h3 class="text-center">{{$booking->nama_penginapan}}</h3></div>
        <div class="mb-3 border p-3 border-radius: 10px; border: 0.50px black solid ">
            
            
            <hr>
            <div class="row">
                <div class="col">
                    Check-in 
                </div>
                <div class="col">
                    Check-out
                </div>
                <div class="col">
                    Type 
                </div>
        </div>
        <div class="row">
            <div class="col">
                <p>
            {{ $checkin }}          
        </p>
    </div>
    <div class="col">
            {{ $checkout }}
        </div>
        <div class="col">
            Villa
        </div>
    </div>
    
</div>
<!-- Tambahkan input lainnya sesuai kebutuhan -->
</div>
</div>
</div>


<button class="btn btn-success d-block mx-auto" type="submit" style="background-color: #57C5B6; border-radius: 5px;">Lanjutkan</button><br>
</form>



@endsection('container')

