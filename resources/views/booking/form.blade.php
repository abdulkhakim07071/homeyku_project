<!DOCTYPE html>
<html>
<head>
    <title>Form Booking Villa</title>
</head>
<body>
    <h2>Form Booking Villa</h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('booking.success') }}">
        @csrf

        <label for="nama">Nama:</label>
        <input type="text" name="nama" id="nama" required>
        <br><br>

        <label for="tanggal_check_in">Tanggal Check-in:</label>
        <input type="date" name="tanggal_check_in" id="tanggal_check_in" required>
        <br><br>

        <label for="tanggal_check_out">Tanggal Check-out:</label>
        <input type="date" name="tanggal_check_out" id="tanggal_check_out" required>
        <br><br>

        <label for="jumlah_orang">Jumlah Orang:</label>
        <input type="number" name="jumlah_orang" id="jumlah_orang" required>
        <br><br>

        <!-- Tambahkan input lainnya sesuai kebutuhan -->

        <input type="submit" value="Submit">
    </form>
</body>
</html>
