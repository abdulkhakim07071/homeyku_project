@extends('layouts.main')
     
@section('container')
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@48,400,0,0" />


<h5 style="padding: 10px; margin: 10px;">Bukti Pesanan Anda</h5>
<div class="row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <div class="card" style="width: 135%;">
            <div class="card-body" >
                <h4 class="card-title">{{ $booking->nama_penginapan }}</h4><br>
                    <div class="card-body d-flex">
                        <img src="img/vila.png" style="border-radius: 10px; width: 300px; height: 200px;">
                        <p class="card-text" style="padding-left: 20px;">
                            <table  class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th scope="col"><span class="material-symbols-rounded">date_range</span></i>Check In</th>
                                        <th scope="col"><span class="material-symbols-rounded">date_range</span>Check Out</th>
                                        <th scope="col"><span class="material-symbols-rounded">home</span>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $checkin }}</td>
                                        <td>{{ $checkout }}</td>
                                        <td>Villa</td>
                                    </tr>
                                </tbody>
                            </table><br>
                        </p>
                    </div><br>
                    <h6>Detail Kamar</h6>
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                            <td>Tipe Tempat Tidur</td>
                            <th>{{ $booking->fasilitas->jumlah_kamar }} {{$booking->fasilitas->tipe_bed}}</th>
                            </tr>
                            <tr>
                            <td>Tamu Per Kamar</td>
                            <th>2 Tamu</th>
                            </tr>
                            <tr>
                            <td>Permintaan Khusus </td>
                            <td></td>
                            </tr>
                        </tbody>
                    </table>
                
            </div>
        </div>
    </div>
    
    <div class="col-sm-4 corner-top-right" style="width: 30%;">
    <div class="card shadow p-3 mb-2 bg-body-tertiary rounded-10">
        <div class="card-body" style="max-height: 300px;">
            <h6 class="card-title-detail">Detail Kontak</h6>
            <p class="card-text">{{ $booking->users->name}}</p>
            <p class="card-text">{{ $booking->users->email}}</p>
        </div>
    </div>
</div>
</div>
<br>

<div class="row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <div class="card" style="width: 135%;">
            <div class="card-body" >
                <h6 class="card-title-detail">Kebijakan Pembatalan</h6>
                    <div class="card-body d-flex">
                        <p class="card-text" style="padding-left: 20px;">
                        Pemesanan ini tidak bisa direfund.
                        Waktu yang ditampilkan sesuai dengan waktu lokal hotel. 
                        Tanggal inap dan tipe kamar tidak dapat diubah  
                        </p>
                    </div>
                
            </div>
        </div>
    </div>
</div><br>

<div class="row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <div class="card" style="width: 135%;">
            <div class="card-body" >
                <h6 class="card-title-detail">Kebijakan Akomodasi</h6>
                    <div class="card-body d-flex">
                        <p class="card-text" style="padding-left: 20px;">
                        <span class="material-symbols-outlined">nest_clock_farsight_analog</span>
                            <div class="row" style="margin-left: 10px">
                                <div >Waktu Check-in / Check-out</div>
                                <div>Check-in:
                                    <p>{{ $checkin }} 14.00</p>
                                </div>
                                <div>Check-out:
                                    <p>{{ $checkout }} 12.00</p>
                                </div>
                            </div>  
                        </p>
                    </div>
                
            </div>
        </div>
    </div>
</div><br>
@php
                $checkinTimestamp = strtotime($checkin);
                $checkoutTimestamp = strtotime($checkout);
                
                // Hitung selisih hari antara checkin dan checkout
$jumlah_hari = ($checkoutTimestamp - $checkinTimestamp) / (60 * 60 * 24);

$harga = $booking->harga * $jumlah_hari;
$pajak = $harga * 10 /100;
$fee = $harga * 5 /100;
$total = $harga + $fee + $pajak;
@endphp

<div><h5>Rincian Harga</h5></div>
<div class="row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        <div class="card" style="width: 135%;">
            <div class="card-body" >
            <div class="row">
    <div class="col-8" >Total biaya</div>
    <div class="col-4"><h5>Rp. {{$total}},00</h5></div>
    </div>
                <hr>
                <div class="row g-0 text-left">
                    <div class="col-sm-6 col-md-8">({{$jumlah_hari}}x) Villa ({{$jumlah_hari}} malam)</div>
                    <div class="col-6 col-md-4">Rp {{ $harga }},00</div>
                    <div class="col-sm-6 col-md-8">Pajak</div>
                    <div class="col-6 col-md-4">Rp {{$pajak}},00</div>
                    <div class="col-sm-6 col-md-8">Platform fee</div>
                    <div class="col-6 col-md-4" style="color: #57C5B6;">Rp {{ $total }},00</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><br>

<div class="row">
    <div class="col-sm-6 mb-3 mb-sm-0">
        
            <div class="card-body" >
                <p>Dengan mengeklik tombol di bawah, Anda menyetujui Syarat dan Ketentuan serta Kebijakan Privasi dari Homeyku.</p>
            </div>
    </div>
</div><br>
<a href="{{route('dashboard')}}">
<button class="btn btn-success d-block mx-auto" type="submit" action="#" style="background-color: #57C5B6; border-radius: 5px;">back</button></a><br>





@endsection('container')

