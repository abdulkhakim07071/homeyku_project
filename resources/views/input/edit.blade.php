      <!-- content -->
      @extends('layouts.input')

@section('container')
<div class="col">
    <form class="jarak" action="{{ route('update', $penginapan->id) }}" method="POST" enctype="multipart/form-data">
      <h2>Input Data Penginapan</h2>
@csrf
@method('PUT')
<div class="form-floating">
  <input type="text" class="form-control @error('nama_penginapan') is-invalid @enderror" id="nama" placeholder="Nama Penginapan" name="nama_penginapan" value="{{ $penginapan->nama_penginapan }}">
  @error('nama_penginapan')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
  <label for="nama">Nama Penginapan</label>
</div>
<div class="form-floating">
  <input type="alamat" class="form-control" id="alamat" name="alamat" placeholder="alamat" value="{{ $penginapan->alamat }}">
  @error('alamat')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
  <label for="alamat">Alamat</label>
</div>
<div class="form-floating">
  <textarea class="form-control" placeholder="Deskripsi" id="deskripsi" style="height: 100px" name="deskripsi">{{ $penginapan->deskripsi }}</textarea>
  @error('deskripsi')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
  <label for="deskripsi">Deskripsi</label>
</div>
      <div class="form-group">
        <input type="file" class="form-control-file" id="gambar" name="images"value="{{ $penginapan->images }}" required>
        @error('images')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <label for="gambar">Gambar</label>
      </div>
      <div class="form-group">
        <label for="type">Tipe Penginapan</label>
        <select class="form-control" id="type" name="tipe">
          <option value="villa" {{ $penginapan->tipe_bed === 'villa' ? 'selected' : '' }}>
            Villa
</option>
          <option value="Hotel" {{ $penginapan->tipe_bed === 'Hotel' ? 'selected' : '' }}>
            Hotel
          </option>
          <option value="Wisma" {{ $penginapan->tipe_bed === 'Wisma' ? 'selected' : '' }}>
            Wisma
          </option>
        </select>
      </div>
      
      <div class="form-floating">
        <input type="text" class="form-control" id="harga" name="harga" placeholder="harga" value="{{ $penginapan->harga }}" required>
        @error('harga')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <label for="harga">Harga</label>
      </div>
    </div>
    
      <div class="col">

        <h3 class="jarak">Fasilitas</h3>
        <div class="form-floating">
          <input type="number" class="form-control" name="jumlah_kamar" id="jumlah_kamar" value="{{ $fasilitas->jumlah_kamar ?? '' }}" required>
          @error('jumlah_kamar')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
          <label for="jumlah_kamar">Jumlah Kamar:</label>

      </div>
      <div class="form-floating">
          <input type="number" class="form-control" name="kamar_mandi" id="kamar_mandi" value="{{ $fasilitas->kamar_mandi ?? '' }}" required>
          @error('kamar_mandi')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
          <label for="kamar_mandi">Kamar Mandi :</label>

      </div>
      <div class="form-group">
        <label for="parkiran">Parkiran</label>
        <select class="form-control" id="parkiran" name="parkiran" value="{{ $fasilitas->parkiran ?? '' }}">
          <option value="luas" {{ $fasilitas->tipe_bed === 'luas' ? 'selected' : '' }}>Luas</option>
          <option value="sedang" {{ $fasilitas->tipe_bed === 'sedang' ? 'selected' : '' }}>Sedang</option>
          <option value="kecil" {{ $fasilitas->tipe_bed === 'kecil' ? 'selected' : '' }}>Kecil</option>
        </select>
      </div>
      <div class="form-group">
        <label for="bed">Tipe Bed</label>
        <select class="form-control" id="bed" name="tipe_bed" value="{{ $fasilitas->tipe_bed ?? '' }}">
          <option value="singlebed" {{ $fasilitas->tipe_bed === 'singlebed' ? 'selected' : '' }}>Single Bed</option>
          <option value="doublebed" {{ $fasilitas->tipe_bed === 'doublebed' ? 'selected' : '' }}>Double Bed</option>
          <option value="twinbed" {{ $fasilitas->tipe_bed === 'twinbed' ? 'selected' : '' }}>Twin Bed</option>
          <option value="kingbed" {{ $fasilitas->tipe_bed === 'kingbed' ? 'selected' : '' }}>King Bed</option>
        </select>
      </div>
      <div class="form-group">
      <div class="form-check"> 
     
        <input type="checkbox" name="ac" id="ac" {{ $fasilitas->ac ? 'checked' : '' }}>
        <label for="ac">  AC</label>
        </div>
      
    </div>
    <div class="form-group">
    <div class="form-check"> 
      
      <input type="checkbox" name="wifi" id="wifi" {{ $fasilitas->wifi ? 'checked' : '' }}>
      <label for="wifi">  WiFi</label>
        </div>
      </div>
      <div class="form-group">
      <div class="form-check"> 
        <input type="checkbox" name="breakfast" id="breakfast" {{ $fasilitas->breakfast ? 'checked' : '' }}>
        <label for="breakfast"> Breakfast</label>
        </div>
                    
      </div>
      <div class="form-group">
      <div class="form-check"> 
        <input type="checkbox" name="kolam_renang" id="kolam_renang" {{ $fasilitas->kolam_renang ? 'checked' : '' }}>
        <label for="kolam_renang">  Kolam Renang</label>
      </div>
       
      </div>
      <div class="form-group">
      <div class="form-check"> 
        <input type="checkbox" name="air_hangat" id="air_hangat" {{ $fasilitas->air_hangat ? 'checked' : '' }}>
        <label for="air_hangat">  Air Hangat</label>
        </div>
            
        
      </div>

      


        
      <div class="jarak">
      <button type="submit" class="btn btn-utama" >Submit</button> <a href="/dashboardadmin"><button type="button" class="btn btn-second">Back</button></a>
      </div>
      </div>
    </form>
@endsection