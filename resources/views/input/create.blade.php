@extends('layouts.input')
@section('container')
    
    <div class="col">
    <form class="jarak" action="/create" method="post" enctype="multipart/form-data">
      <h2>Input Data Penginapan</h2>
@csrf
<div class="form-floating">
  <input type="text" class="form-control @error('nama_penginapan') is-invalid @enderror" id="nama" placeholder="Nama Penginapan" name="nama_penginapan" value="{{ old('nama_penginapan')}}">
  @error('nama_penginapan')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
  <label for="nama">Nama Penginapan</label>
</div>
<div class="form-floating">
  <input type="alamat" class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat" placeholder="alamat" value="{{ old('alamat')}}">
  @error('alamat')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
  <label for="alamat">Alamat</label>
</div>
<div class="form-floating">
  <textarea class="form-control @error('deskripsi') is-invalid @enderror" placeholder="Deskripsi" id="deskripsi" style="height: 100px" name="deskripsi" >{{ old('deskripsi')}}</textarea>
  @error('deskripsi')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
  <label for="deskripsi">Deskripsi</label>
</div>
      <div class="form-group">
        <input type="file" class="form-control-file @error('images') is-invalid @enderror" id="gambar" name="images[]"  value="{{ old('images')}}" multiple required>
        @error('images')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
     
      </div>
      <div class="form-group">
        <label for="tipe">Tipe Penginapan</label>
        <select class="form-control  @error('tipe') is-invalid @enderror" id="tipe" name="tipe">
          <option value="villa" @if(old('tipe') == 'villa') selected @endif>
            Villa
</option>
          <option value="Hotel" @if(old('tipe') == 'Hotel') selected @endif>
            Hotel
          </option>
          <option value="Wisma" @if(old('tipe') == 'Wisma') selected @endif>
            Wisma
          </option>
        </select>
        @error('tipe')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-floating">
        <input type="text" class="form-control  @error('harga') is-invalid @enderror" id="harga" name="harga" placeholder="harga" value="{{ old('harga')}}" required>
        @error('harga')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <label for="harga">Harga</label>
      </div>
    </div>
      <div class="col">

        <h3 class="jarak">Fasilitas</h3>
        <div class="form-floating">
        <input type="number" class="form-control @error('jumlah_kamar') is-invalid @enderror" id="jumlah_kamar" name="jumlah_kamar" value="{{ old('jumlah_kamar')}}"placeholder="Kamar">
        @error('jumlah_kamar')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <label for="jumlah_kamar">Jumlah Kamar</label>
      </div>
      <div class="form-floating">
        <input type="number" class="form-control @error('kamar_mandi') is-invalid @enderror" id="kamar_mandi" name="kamar_mandi" value="{{ old('kamar_mandi')}}"placeholder="Kamar">
        @error('kamar_mandi')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
        <label for="kamar_mandi">Kamar Mandi</label>
      </div>
      <div class="form-group">
        <label for="parkiran">Parkiran</label>
        <select class="form-control  @error('parkiran') is-invalid @enderror" id="parkiran" name="parkiran">
          <option value="luas" @if(old('parkiran') == 'luas') selected @endif>Luas</option>
          <option value="sedang" @if(old('parkiran') == 'sedang') selected @endif>Sedang</option>
          <option value="kecil" @if(old('parkiran') == 'kecil') selected @endif>Kecil</option>
        </select>
        @error('parkiran')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bed">Tipe Bed</label>
        <select class="form-control @error('tipe_bed') is-invalid @enderror" id="bed" name="tipe_bed">
          <option value="singlebed" @if(old('tipe_bed') == 'singlebed') selected @endif>Single Bed</option>
          <option value="doublebed" @if(old('tipe_bed') == 'doublebed') selected @endif>Double Bed</option>
          <option value="twinbed" @if(old('tipe_bed') == 'twinbed') selected @endif>Twin Bed</option>
          <option value="kingbed" @if(old('tipe_bed') == 'kingbed') selected @endif>King Bed</option>
        </select>
        @error('tipe_bed')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>

      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="ac" id="ac" value="true" @if(old('ac')=='true')) checked @endif>
          <label class="form-check-label" for="ac">AC</label>
        </div>
      </div>
      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="wifi" id="wifi" value="true" @if(old('wifi')=='true')) checked @endif>
          <label class="form-check-label" for="wifi">WiFi</label>
        </div>
      </div>
      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" id="kolam_renang" name="kolam_renang" value="true" @if(old('kolam_renang')=='true')) checked @endif>
          <label class="form-check-label" for="kolam_renang">Kolam Renang</label>
        </div>
      </div>
      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" id="air_hangat" name="air_hangat" value="true" @if(old('air_hangat')=='true')) checked @endif>
          <label class="form-check-label" for="air_hangat">Air Hangat</label>
        </div>
      </div>
      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" id="breakfas" name="breakfast" value="true" @if(old('breakfast')=='true')) checked @endif>
          <label class="form-check-label" for="breakfas">Breakfast</label>
        </div>
      </div>
        
      </div>
      <div class="jarak">
        <input type="hidden" name="id_admin" value=" {{ auth()->user()->id }}">
      <button type="submit" class="btn btn-utama" >Submit</button> <a href="/dashboardadmin"><button type="button" class="btn btn-second">Back</button></a>
      </div>
    </form>
@endsection
