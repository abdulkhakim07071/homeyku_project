
      <!-- content -->
      @extends('layouts.admin')

@section('container')
      <div class="col-md-8">
        <div class="container content3">
          <div class="row">
          @if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session('success') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif  
@if(session()->has('loginError'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  {{ session('loginError') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif  
            <h4>Daftar Penginapas</h4>
          </div>
          <table class="table table-striped-columns">
            <thead>
            <tr>
              <th> Nama</th>
              <th> Alamat </th>
              <th> Harga </th>
              <th>  </th>
              <th>  </th>
            </tr>
            </thead>
            <tbody>
          @foreach($penginapans as $data)
                <tr>
                    <td>{{ $data->nama_penginapan }}</td>
                    <td>{{ $data->alamat }}</td>
                    <td> Rp.{{ $data->harga }}</td>
                    <td>
            <a href="{{ route('edit', $data->id) }}" class="btn btn-primary">Edit</a>
                    </td>
                    <td>
            <form action="{{ route('delete', $data->id) }}" method="post"  enctype="multipart/form-data" style="display:inline">
                @csrf
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
                </tr>
                @endforeach
    
            </tbody>
    </table>
       

        </div>

        <br>
        <div class="center">
        <a href="/create"><button class="btn btn-utama">Tambah Data Penginapan</button></a>
        </div>
      </div>
@endsection