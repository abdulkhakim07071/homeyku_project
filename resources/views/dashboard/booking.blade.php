
      <!-- content -->
      @extends('layouts.admin')

@section('container')
      <div class="col-md-8">
      <div class="container content3">

        <div class="row">
            <h4>Booking</h4>
      </div>
      <div class="row">
      <table class="table table-striped table-hover" id="booking-table">
    <thead>
        <tr>
        <th>Tanggal</th>    
            <th>Nama Penginapan</th>
            <th>Nama User</th>
            <th>Check-in</th>
            <th>Check-out</th>
            <th>Metode Pembayaran</th>
            <th>Jumlah Pembayaran</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($booking as $data)
        @php
        $currentDate = date('Y-m-d'); // Tanggal sekarang
        $itemDate = $data->tanggal; // Tanggal pada item

        if (strtotime($itemDate) >= strtotime($currentDate)) {
            @endphp
            
            <tr>
                <td>{{ $data->tanggal }}</td>
                <td>{{ $data->nama_penginapan }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->check_in }}</td>
                <td>{{ $data->check_out }}</td>
                <td>{{ $data->metode_pembayaran }}</td>
                <td>{{ $data->jumlah_pembayaran }}</td>
                <td>{{ $data->status }}</td>
            </tr>
            @php
        }
            @endphp
        @endforeach
    </tbody>
</table>

      </div>
      </div>
      <div class="container content3">

        <div class="row">
            <h4>Riwayat Booking</h4>
      </div>
      <div class="row">
      <table class="table table-striped table-hover" id="booking-table">
    <thead>
        <tr>
        <th>Tanggal</th>    
            <th>Nama Penginapan</th>
            <th>Nama User</th>
            <th>Check-in</th>
            <th>Check-out</th>
            <th>Metode Pembayaran</th>
            <th>Jumlah Pembayaran</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($booking as $data)
        @php
        $currentDate = date('Y-m-d'); // Tanggal sekarang
        $itemDate = $data->tanggal; // Tanggal pada item

        if (strtotime($itemDate) < strtotime($currentDate)) {
           @endphp
            <tr>
                <td>{{ $data->tanggal }}</td>
                <td>{{ $data->nama_penginapan }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->check_in }}</td>
                <td>{{ $data->check_out }}</td>
                <td>{{ $data->metode_pembayaran }}</td>
                <td>{{ $data->jumlah_pembayaran }}</td>
                <td>{{ $data->status }}</td>
            </tr>
            @php
        }
            @endphp
        @endforeach
    </tbody>
</table>

      </div>
      </div>
      </div>
    <!-- jQuery and DataTables CDN -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#booking-table').DataTable({
            "paging": false,
            "searching": false,
            "info": false,
            "order": [[ 0, "asc" ]],
            "columnDefs": [
                { "orderable": false, "targets": [1, 2, 3, 4, 5, 6] }
            ]
        });
    });
</script>
@endsection