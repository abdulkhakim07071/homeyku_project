@extends('layouts.main')

@section('container')
        <!-- Carrousel -->
        <div class="container-fluid bg-header">
            <div class="container">
        <div class="row">
             <div class="col">
                <h1> Booking Penginapanmu Di Kaliurang Sekarang </h1>
                <h5>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </h5>
                <button type="button" class="btn btn-utama">Login</button>
             </div>
             <div class="col">
                <img src="img/header.png">
               
             </div>
         </div>
        </div>
        </div>
    </div>

    <!-- Navbar & Carousel End -->
    <!-- Search -->
    <div class="row-container">
        <form action="{{ route('searchajax') }}" method="GET">
        <div class="search">
            
    <div class="row justify-content-md-center">
    <div class="col-auto form-border">
        <label for="location"><i class="bi bi-geo-alt"></i>  Lokasi </label>
        <input name="location" type="text" class="form-control" id="location" placeholder="Masukkan lokasi" value="{{ request('lokasi') }}">
    </div>
    <div class="col-auto form-border">

        <label for="checkin"><i class="bi bi-calendar-event"></i>  Tanggal Check-in </label>
           

        <input name="checkin" type="date" class="form-control" id="checkin" value="{{ request('checkin') }}">

    </div>
    <div class="col col-lg-2 form-border">

        <label for="checkout"><i class="bi bi-calendar-event"></i>  Tanggal Check-out </label>

        <input name="checkout" type="date" class="form-control" id="checkout" value="{{ request('checkout') }}">

    </div>
    <div class="col col-lg-2 form-border">
    <label for="type">Tipe Penginapan</label>
            <select class="form-control" id="type" name="tipe">
                <option value="villa" {{ old('tipe') === 'villa' ? 'selected' : '' }}>
                    Villa
                </option>
                <option value="hotel" {{ old('tipe') === 'hotel' ? 'selected' : '' }}>
                    Hotel
                </option>
                <option value="wisma" {{ old('tipe') === 'wisma' ? 'selected' : '' }}>
                    Wisma
                </option>
            </select>

    </div>
  </div>
  <div class="row">
  <div class="col 1-4"></div>
  <div class="col 5-8"></div>
  <div class="col 9-10"></div>
  <div class="col 10-12"></div>
    <div class="col 10-12">
        <button type="submit" class="btn btn-utama">Search</button>
    </div>
  
  </div>
        </div>
        </form>
    </div>
    <!-- search end -->


    <!-- content -->
    <div class="container-fluid">
        <div class="conatiner">
            <!-- content1 -->
            <div class="row jarak">
                <h1 class="judul-content">
                    Destinasi Top Kaliurang
                </h1>
            </div>
            <div class="row jarak">
                <div class="col content1">
                    <img src="img/sentalu.png">
                    <h6 >
                        Musseum Ullen Sentalu
                    </h6>
                </div>
                <div class="col content1">
                    <img src="img/lava.png">
                    <h6>
                        Lava Tour Merapi
                    </h6>
                </div>
                <div class="col content1">
                    <img src="img/bunker.png">
                    <h6 >
                        Bunker Kaliadem
                    </h6>
                </div>
                <div class="col content1">
                    <img src="img/castle.png">
                    <h6 >
                        The Lost World Castle
                    </h6>
                </div>
            </div>
            <hr>

            <!-- content2 -->
            <div class="row jarak">
                <div class="col content2">
                    <h2>
                    Penginapan Dan Destinasi<br>
                    Populer di Kaliurang 
                    </h2>
                    <h6>
                    Lorem ipsum dolor sit amet consectetur. <br>
                    Justo eget tellus elit gravida orci ipsum mauris ut quam. <br>
                    Duis nisi fringilla urna eget eu feugiat.<br>
                     Pharetra ullamcorper laoreet enim consectetur. 
                    </h6>
                    <button type="Login" class="btn btn-utama">Login</button>
                </div>
                <div class="col">
                    <img src="img/destinasi1.png">
                    <img src="img/destinasi2.png">
                </div>
            </div>
<hr>
            <!-- content3 -->
            <div class="row">
                <h1 class="judul-content">
                    Apa Kata Mereka ?
                </h1>
                <div class="col content3">
                    <img src="img/content3.png">
                    <p>
                    Lorem ipsum dolor sit amet<br>
                    consectetur.Cursus et tincidunt<br>
                    enim morbi luctus sapien <br>
                    ullamcorper. Mauris lobortis<br>
                    nibh et magna arcu. Integer <br>
                    nibh ac lacus ante eget. Id <br>
                    malesuada lorem eu leovitae<br>
                     id nunc ac at.
                    </p>
                </div>
                <div class="col content3">
                    <img src="img/content3.png">
                    <p>
                    Lorem ipsum dolor sit amet<br>
                    consectetur.Cursus et tincidunt<br>
                    enim morbi luctus sapien <br>
                    ullamcorper. Mauris lobortis<br>
                    nibh et magna arcu. Integer <br>
                    nibh ac lacus ante eget. Id <br>
                    malesuada lorem eu leovitae<br>
                     id nunc ac at.
                    </p>
                </div>
                <div class="col content3">
                    <img src="img/content3.png">
                    <p>
                    Lorem ipsum dolor sit amet<br>
                    consectetur.Cursus et tincidunt<br>
                    enim morbi luctus sapien <br>
                    ullamcorper. Mauris lobortis<br>
                    nibh et magna arcu. Integer <br>
                    nibh ac lacus ante eget. Id <br>
                    malesuada lorem eu leovitae<br>
                     id nunc ac at.
                    </p>
                </div>
                <div class="col content3">
                    <img src="img/content3.png">
                    <p>
                    Lorem ipsum dolor sit amet<br>
                    consectetur.Cursus et tincidunt<br>
                    enim morbi luctus sapien <br>
                    ullamcorper. Mauris lobortis<br>
                    nibh et magna arcu. Integer <br>
                    nibh ac lacus ante eget. Id <br>
                    malesuada lorem eu leovitae<br>
                     id nunc ac at.
                    </p>
                </div>

            </div>


            <!-- content 4 -->
            <div class="row color-content4">
                <div class="col">
                    <img src="img/content4.png">
                </div>
                <div class="col content4">
                    <H1>
                        HOMEYKU
                    </H1>
                    <p>
                    Hemat waktu , Hemat Uang <br>
Daftar dan kami akan mengirimkan penawaran terbaik untuk Anda<br>
Masukkan alamat email Anda dan kami akan mengirimkan penawaran terbaik kami
                    </p>
                    <div class="row">
                        <div class="col">
                            <input type="email" class="form-control" id="email" placeholder="email">
                        </div>
                        <div class="col">
                            <button type="Gabung" class="btn btn-utama">Gabung</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content end -->
   <!-- resources/views/dashboard.blade.php -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


@endsection