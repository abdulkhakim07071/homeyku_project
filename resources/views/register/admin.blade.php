@extends('layouts.login')
@section('container')
<div class="row justify-content-center">
      <div class="col-lg-8">
               <div class="card">
    <form action="/adminregister" method="post">
      @csrf
      <div class="form-group" style="padding: 15px; ">

        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Masukkan Nama" required>
        @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group" style="padding: 15px; ">

        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Masukkan Email" required value="{{ old ('email') }}">
        @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-group" style="padding: 15px; ">

        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Masukkan Password" required>
        @error('password')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="col-lg center" style="padding: 10px; ">
      <input type="hidden" name="role" value="admin">
            <button type="submit" class="btn btn-utama" >Daftar </button>
      </div>
    </form>
               </div>
      </div>
</div>

@endsection