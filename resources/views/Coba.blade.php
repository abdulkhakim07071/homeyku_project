<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <title>Kolom Login</title>
</head>

<body>
  <div class="container">
    <div class="row justify-content-center mt-5">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h4 class="text-center">Login</h4>
          </div>
          <div class="card-body">
            <form>
              <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Masukkan email">
              </div>
              <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Masukkan password">
              </div>
              <button type="submit" class="btn btn-primary btn-block">Login</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
 <ul id="searchResult"></ul>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <div class="container">
        @foreach($penginapan as $data)
            <div class="row">
                <div class="col form-border">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center justify-content-center">
                            <img src="img/vila.png" class="img-fluid rounded " alt="Foto 1">
                        </div>
                        <div class="col-md-3">
                            <div class="card-body">
                                <h5 class="card-title" style="font-family: 'Sorts Mill Goudy', serif;">{{$data->nama_penginapan}}</h5>
                                <p style="font-family: 'Sorts Mill Goudy', serif;">
                                    <i class="bi bi-person me-2"></i>
                                    <span class="small">max 10 orang</span>
                                </p>
                                <p style="font-family: 'Sorts Mill Goudy', serif;">
                                    <i class="fas fa-bed me-2"></i>
                                    <span class="small">{{$data->jumlah_kamar}} kamar tidur</span>
                                </p>
                                <p style="font-family: 'Sorts Mill Goudy', serif;">
                                    <i class="fas fa-shower me-2"></i>
                                    <span class="small">{{$data->kamar_mandi}} kamar mandi</span> 
                                </p>
                            </div>
</div>
                            <div class="col ">
                            <div class="d-flex justify-content-start ">
                                <div class="text-end md-12" style="text-align: right;padding-top: 75px;">
                                    <h4><p class="small text-decoration-line-through" style="font-family: 'Sorts Mill Goudy', serif;">Rp. {{$data->harga+($data->harga/100*10)}},00</p></h4>
                                    <p class="h4 fw-bold text-warning" style="font-family: 'Sorts Mill Goudy', serif;">Rp. {{$data->harga}},00</p>
                                </div>
                            </div>
</div>
<div class="col ">
                            <div class="rating " style="padding-top: 100px; ">
                            @php
                                $rating = number_format($data->averageRating(), 1);
                            @endphp
                            <p>{{$rating}}/5<p>
                            @if($rating)
                            @for($i = 1; $i <= 5; $i++)
                                @if($i <= $rating)
                                    <i class="bi bi-star-fill text-warning"></i>
                                @elseif($i == ceil($rating) && $rating != floor($rating))
                                     <i class="bi bi-star-half text-warning"></i>
                                @else
                                     <i class="bi bi-star text-warning"></i>
                                @endif
                            @endfor

                        @endif
                        @if($rating == 0.0)
                                <br><p>Belum Ada Rating</P>
                            @endif

</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach 
    </div>
</html>