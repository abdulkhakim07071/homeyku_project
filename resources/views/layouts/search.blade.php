<!DOCTYPE html>
<html>
    <head>
        
       
        <meta charset="utf-8">
        <title>
            Dashboard
        </title>

        <!-- Customized Bootstrap Stylesheet -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->

        <!-- CSS -->
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <script src="{{asset('js/search.js')}}" rel="javascript/text"></script>

    <body>
    <!-- Navbar -->
    
    @include('layouts.partials.navbar')

    <div class="container-fluid">
    <div class="row-container">
        <div class="search">
        <form id="search-form">
    @csrf
    <div class="row justify-content-md-center">
        <div class="col-auto form-border">
            <label for="location"><i class="bi bi-geo-alt"></i> Lokasi</label>
            <input type="text" class="form-control" id="location" placeholder="Masukkan lokasi" name="location" value="{{ old('location') }}">
        </div>
        <div class="col-auto form-border">
            <label for="checkin"><i class="bi bi-calendar-event"></i> Tanggal Check-in</label>
            <input type="date" class="form-control" id="checkin" name="checkin" value="{{ old('checkin') }}" min="{{ now()->format('Y-m-d') }}">
        </div>
        <div class="col col-lg-2 form-border">
            <label for="checkout"><i class="bi bi-calendar-event"></i> Tanggal Check-out</label>
            <input type="date" class="form-control" id="checkout" name="checkout" value="{{ old('checkout') }}" min="{{ date('Y-m-d', strtotime('+1 day', strtotime(now()->format('Y-m-d')))) }}">

        </div>
        <div class="col col-lg-2 form-border">
            <label for="type">Tipe Penginapan</label>
            <select class="form-control" id="type" name="tipe">
                <option value="villa" {{ old('tipe') === 'villa' ? 'selected' : '' }}>
                    Villa
                </option>
                <option value="hotel" {{ old('tipe') === 'hotel' ? 'selected' : '' }}>
                    Hotel
                </option>
                <option value="wisma" {{ old('tipe') === 'wisma' ? 'selected' : '' }}>
                    Wisma
                </option>
            </select>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <button type="button" id="searchbutton" class="btn btn-utama">Submit</button>
        </div>
    </div>
</form>

</div>
        </div>

    </div>
        <!-- content -->
    <div class="container-fluid">
        @yield('container')
    </div>
    
    <!-- footer -->
    @include('layouts.partials.footer')
    <div id="result">
        <ul id="memlist">

        </ul>
    </div>

@yield('script')
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
</html>
