<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
         <!-- Customized Bootstrap Stylesheet -->
         <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSS -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
                <div class="row">
                <div class="col login1">
                    <img src="img/discount.png">
                    <img src="img/login.png">
                    <h2 class="content1">
                        Dapatkan Discount Sekarang Juga!!
                    </h2>
                    <p class="content1">
                    Lorem ipsum dolor sit amet,<br>
                     consectetur adipiscing elit, <br>
                     sed do eiusmod tempor incididunt
                     <br> ut labore et dolore<br>
                      magna aliqua.
                    </p>
                </div>
                <div class="col">
                
                <div class="row center">
                    <img src="img/logo.png" weight="20px"class="content1">
                </div>
                <div class="row">
                    <h3 class="judul-content">
                        Login/Register
                    </h3>
                </div>
                    @yield('container')
                </div>
        </div>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
</html>