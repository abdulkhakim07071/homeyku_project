<!DOCTYPE html>
<html>
    <head>
        
       
        <meta charset="utf-8">
        <title>
            Dashboard
        </title>

        <!-- Customized Bootstrap Stylesheet -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSS -->
        <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <body>
    <!-- Navbar -->
    
    @include('layouts.partials.navbar')
        <!-- content -->
    <div class="container-fluid">
        @yield('container')
    </div>
    @yield('script')
    <!-- footer -->
    @include('layouts.partials.footer')

    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
</html>
