<header>
        <nav class="navbar navigasi">
            <img src="{{ asset('img/logo.png') }}">
            
            @auth
            <ul class="nav justify-content-end">
                    <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle navigasi-active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Welcome Back, {{ auth()->user()->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/dashboardadmin"><i class="bi bi-layout-text-sidebar-reverse"></i> My Dashboard</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li>
                            <form action="/logoutadmin" method="post">
                                @csrf
                                <button type="submit" class="dropdown-item"><i class="bi bi-box-arrow-right"></i> Logout</button>
                            </form>
                    </ul>
                    </li>
            </ul>
            @endauth
            
            
                </div>  
            
            </div>
            
        </nav>
        </header>