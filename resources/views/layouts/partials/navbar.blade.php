
<nav class="navbar navigasi">
            <img src="img/logo.png"> 
            <ul class="nav justify-content-end">
                <li class="nav-item ">
                    <a class="nav-link active navigasi-active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link navigasi-nonactive" href="#">Booking</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link navigasi-nonactive">Contact</a>
                </li>

                
                @auth
                    <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Welcome Back, {{ auth()->user()->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/dashboard"><i class="bi bi-layout-text-sidebar-reverse"></i> My Dashboard</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li>
                            <form action="/logout" method="post">
                                @csrf
                                <button type="submit" class="dropdown-item"><i class="bi bi-box-arrow-right"></i> Logout</button>
                            </form>
                    </ul>
                    </li>
                    @else

                <li class="nav-item navigasi-active">
                    <a class="nav-link navigasi-nonactive" href="/login"> Login </a>
                </li>
                <li class="nav-item navigasi-active">
                    <a class="nav-link navigasi-nonactive" href="login"> Daftar </a>
                </li>
            @endauth
            </ul>
            
                </div>  
            
            </div>
            
        </nav>