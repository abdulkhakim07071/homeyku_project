<div class="col-md-3">
      <sidebar>
        <div class="d-flex flex-column" style="width: 280px;">
    <ul class="nav nav-pills flex-column mb-auto side">
      <li class="nav-item">
        <a href="{{route('dashboardadmin')}}" class="nav-link" aria-current="page">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"></use></svg>
          Room
        </a>
      </li>
      <li>
        <a href="{{route('booking')}}" class="nav-link link-dark">
          <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
          Booking
        </a>
      </li>
    </ul>
  </div>
        </sidebar>
      </div>