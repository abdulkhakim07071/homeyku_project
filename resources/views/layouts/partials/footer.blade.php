 <!-- footer -->
 <div class="container-fluid footer-bg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h5>
                        Tentang HOMEYKU
                    </h5>
                    <ul>
                        <li>Tentang Kami</li>
                        <li>Hubungi Kami</li>
                        <li>Sitemap</li>
                    </ul>
                </div>
                <div class="col">
                <h5>
                        Info
                    </h5>
                    <ul>
                        <li>T & C</li>
                        <li>Ketentuan dan Privasi</li>
                        <li>Ketentuan Cookie</li>
                        <li>Tanya Jawab</li>
                        <li>Agent Registration</li>
                        <li>Komunitas Pegiat Wisata Kaliurang</li>
                    </ul>
                </div>
                <div class="col">
                <h5>
                        Situs Global
                    </h5>
                    <ul>
                        <li>India</li>
                        <li>Singapura</li>
                        <li>Malaysia</li>
                        <li>Indonesia</li>
                        <li>Peru</li>
                        <li>Colombia</li>
                    </ul>
                </div>
                <div class="col">
                <h5>
                        Our Partners
                    </h5>
                    <ul>
                        <li>Gojek</li>
                        <li>Grab</li>
                    </ul>
                </div>
                <div class="col">
                <h5>
                        <img src="img/logo2.jpg" >
                    </h5>
                    <p>
                    Lorem ipsum dolor sit amet consectetur. Varius eu neque blandit neque pellentesque sed id molestie odio. Fames vitae scelerisque in aliquam quam a urna. Euismod id malesuada eget in. Aliquam aenean quis pellentesque aliquam.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">

                </div>
                <div class="col">
                    
                </div>
                <div class="col">
                    
                </div>
                <div class="col">
                    <p ><i class="bi bi-c-circle copyright"></i> 2023 HOMEYKU All rights reserved</p>
                </div>
            </div>
        </div>
    </div>