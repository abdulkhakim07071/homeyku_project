<!DOCTYPE html>
<html>
    <head>
        <title>
        <?= $title ?>
              </title>
               <!-- Customized Bootstrap Stylesheet -->
               <link href="css/bootstrap.min.css" rel="stylesheet">
             <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">


    </head>
    <body>
    @include('layouts.partials.headadmin')

    <div class="container-fluid">
    <div class="row">
    @include('layouts.partials.sidebar')
      <!-- content -->
      @yield('container')
    </div>
  </div>
      

    </body>
    <!-- Tambahkan jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<!-- Tambahkan DataTables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

</html>