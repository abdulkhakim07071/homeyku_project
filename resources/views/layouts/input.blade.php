<!DOCTYPE html>
<html>
    <head>
        <title>
 
    </title>
               <!-- Customized Bootstrap Stylesheet -->
               <link href="css/bootstrap.min.css" rel="stylesheet">
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    </head>
    <body>
    @include('layouts.partials.headadmin')

    <div class="container row">
    @yield('container')
  </div>
    </body>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>