@extends('layouts.main')

@section('container')

    <!-- Search -->
    <div class="row-container">
        <div class="search">
        <form id="searchForm" >
    
    <div class="row justify-content-md-center">
        <div class="col-auto form-border">
            <label for="location"><i class="bi bi-geo-alt"></i> Lokasi</label>
            <input type="text" class="form-control" id="location" placeholder="Masukkan lokasi" name="location" value="{{ old('location') }}">
        </div>
        <div class="col-auto form-border">
            <label for="checkin"><i class="bi bi-calendar-event"></i> Tanggal Check-in</label>
            <input type="date" class="form-control" id="checkin" name="checkin" value="{{ old('checkin') }}" min="{{ now()->format('Y-m-d') }}">
        </div>
        <div class="col col-lg-2 form-border">
            <label for="checkout"><i class="bi bi-calendar-event"></i> Tanggal Check-out</label>
            <input type="date" class="form-control" id="checkout" name="checkout" value="{{ old('checkout') }}" min="{{ date('Y-m-d', strtotime('+1 day', strtotime(now()->format('Y-m-d')))) }}">

        </div>
        <div class="col col-lg-2 form-border">
            <label for="type">Tipe Penginapan</label>
            <select class="form-control" id="type" name="tipe">
                <option value="villa" {{ old('tipe') === 'villa' ? 'selected' : '' }}>
                    Villa
                </option>
                <option value="hotel" {{ old('tipe') === 'hotel' ? 'selected' : '' }}>
                    Hotel
                </option>
                <option value="wisma" {{ old('tipe') === 'wisma' ? 'selected' : '' }}>
                    Wisma
                </option>
            </select>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <button type="submit" class="btn btn-utama">Submit</button>
        </div>
    </div>
</form>

</div>
        </div>
    </div>





    <!-- Search end -->

    <!-- Ini isi -->

        <!-- resources/views/search.blade.php -->
        <div id="searchResults" class="container-fluid">
            <div class="container-fluid">
                @foreach($penginapan as $data)
                @php
                
                $image = $data->image;
    $penginapanImages = json_decode($data->image);
    $penginapanImage = urlencode($penginapanImages[0]);

    @endphp
    <div class="row">
        <a href="{{ route('detail', $data->id) }}">
            <div class="col form-border">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center justify-content-center">
                        <img src="{{ asset('img/data/'.$penginapanImage) }}" class="img-fluid rounded justify-content-start" height="100px" width="200px" alt="Foto 1">
                    </div>
                    <div class="col-md-3">
                        <div class="card-body">
                            <h5 class="card-title" style="font-family: 'Sorts Mill Goudy', serif;">{{$data->nama_penginapan}}</h5>
                            <p style="font-family: 'Sorts Mill Goudy', serif;">
                                <i class="bi bi-person me-2"></i>
                                <span class="small">max {{$data->fasilitas->jumlah_kamar*2}} orang</span>
                            </p>
                            <p style="font-family: 'Sorts Mill Goudy', serif;">
                                <i class="fas fa-bed me-2"></i>
                                <span class="small">{{$data->fasilitas->jumlah_kamar}} kamar tidur</span>
                            </p>
                            <p style="font-family: 'Sorts Mill Goudy', serif;">
                                <i class="fas fa-shower me-2"></i>
                                <span class="small">{{$data->fasilitas->kamar_mandi}} kamar mandi</span> 
                            </p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="d-flex justify-content-center">
                            <div class="text-end md-12" style="text-align: right;padding-top: 45px;">
                                <h4><p class="small text-decoration-line-through" style="font-family: 'Sorts Mill Goudy', serif;">Rp. {{$data->harga+($data->harga/100*10)}},00</p></h4>
                                <p class="h4 fw-bold text-warning" style="font-family: 'Sorts Mill Goudy', serif;">Rp. {{$data->harga}},00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
@endforeach

            </div>
</div>

   

@endsection
@section('script')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
     
    $(document).ready(function() {
        $('#searchForm').on('submit', function(e) {
            e.preventDefault();
            var location = $('#location').val();
            var checkin = $('#checkin').val();
            var checkout = $('#checkout').val();
            var jenis = $('#type').val();

            // Menampilkan data yang akan dikirim ke controller di konsol browser
         
        
            $.ajax({
                type: 'get',
                url: "{{route('searchajax')}}",
                data: $(this).serialize(),
  
                success: function(data) {
                    // Tampilkan data respon dari server di konsol browser

                    // Kode untuk menampilkan hasil pencarian
                    var resultsDiv = $('#searchResults');
                     resultsDiv.empty();
                     var html = '';
                    if (data.length > 0) {
                        // Iterate through the search results and append them to the resultsDiv
                        console.log(data);

    data.forEach(function(penginapan) {
        var imageUrl = '/img/data/' + mainImage;
        resultsDiv.append(`<div class="container-fluid"><div class="row">
        
        <a href="/detail/${penginapan.id}">            
        <div class="col form-border">
                        <div class="row">
                            <div class="col-md-4 d-flex align-items-center justify-content-center">
                            <img src="src="" class="img-fluid rounded justify-content-start" height="100px" width="200px"  alt="Foto 1">
                            </div>
                            <div class="col-md-3">
                                <div class="card-body">
                                    <h5 class="card-title" style="font-family: 'Sorts Mill Goudy', serif;">${penginapan.nama_penginapan}</h5>
                                    <p style="font-family: 'Sorts Mill Goudy', serif;">
                                        <i class="bi bi-person me-2"></i>
                                        <span class="small">max ${penginapan.fasilitas.jumlah_kamar*2} orang</span>
                                    </p>
                                    <p style="font-family: 'Sorts Mill Goudy', serif;">
                                        <i class="fas fa-bed me-2"></i>
                                        <span class="small">${penginapan.fasilitas.jumlah_kamar} kamar tidur</span>
                                    </p>
                                    <p style="font-family: 'Sorts Mill Goudy', serif;">
                                        <i class="fas fa-shower me-2"></i>
                                        <span class="small">${penginapan.fasilitas.kamar_mandi} kamar mandi</span> 
                                    </p>
                                </div>
                            </div>
                            <div class="col">
                                <div class="d-flex justify-content-center">
                                    <div class="text-end md-12" style="text-align: right;padding-top: 45px;">
                                        <h4><p class="small text-decoration-line-through" style="font-family: 'Sorts Mill Goudy', serif;">Rp. ${penginapan.harga+(penginapan.harga/100*10)},00</p></h4>
                                        <p class="h4 fw-bold text-warning" style="font-family: 'Sorts Mill Goudy', serif;">Rp. ${penginapan.harga},00</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </a>
                </div>
                </div>`);
;

        // Add more lines to display other attributes as needed
    });


                    } else {
                        resultsDiv.append('<p>No results found.</p>');
                    }
                },
                error: function(xhr, status, error) {
                    // Tampilkan pesan kesalahan dari server di konsol browser
                    console.log(xhr.responseText);

                    // Kode untuk menampilkan pesan error jika terjadi masalah
                    alert('Terjadi kesalahan saat melakukan pencarian. Silakan coba lagi.');
                }

            });
        });
    });
</script>

@endsection
