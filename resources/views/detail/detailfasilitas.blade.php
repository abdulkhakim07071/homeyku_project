@extends('layouts.main')


@section('container')
<hr>


<!-- Menghubungkan dengan file CSS Bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css">
<!-- Menghubungkan dengan Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cabin&family=Nunito+Sans:wght@200&family=Nunito:ital,wght@0,200;0,400;0,500;1,200&family=Sorts+Mill+Goudy&display=swap" rel="stylesheet">
<div class="container">
<div class="row">

    <div class="photo-container">
        <img src="img/vila.png" alt="Photo 1">
        <img src="img/kamartidur1.png" alt="Photo 2">
        <img src="img/vila.png" alt="Photo 3">
    </div>
 </div>
 <div class="row">
    <div class ="col-9">


<br>
 <h1>Arunika Villas</h1>
    <div class="address">
        Jl. Naga, Kaliurang, Hargobinangun, Kec. Pakem, Kabupaten Sleman,Daerah Istimewa Yogyakarta 55582<br>
    </div>

</br>

<div class="btn-group" role="group" aria-label="Basic radio toggle button group">
  <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
  <label class="btn btn-outline-primary" for="btnradio1">deskripsi</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
  <label class="btn btn-outline-primary" for="btnradio2">fasilitas</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
  <label class="btn btn-outline-primary" for="btnradio3">Rating & Ulasan</label>
</div>
<div>
    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="24" viewBox="0 0 26 24" fill="none">
        <path d="M12.585 12C11.4314 12 10.4439 11.6083 9.62237 10.825C8.80088 10.0417 8.39014 9.1 8.39014 8C8.39014 6.9 8.80088 5.95833 9.62237 5.175C10.4439 4.39167 11.4314 4 12.585 4C13.7385 4 14.7261 4.39167 15.5476 5.175C16.369 5.95833 16.7798 6.9 16.7798 8C16.7798 9.1 16.369 10.0417 15.5476 10.825C14.7261 11.6083 13.7385 12 12.585 12ZM18.8772 20H6.29273C5.71594 20 5.222 19.804 4.8109 19.412C4.39981 19.02 4.19462 18.5493 4.19531 18V17.2C4.19531 16.6333 4.34843 16.1123 4.65465 15.637C4.96087 15.1617 5.36707 14.7993 5.87324 14.55C6.95691 14.0333 8.05805 13.6457 9.17667 13.387C10.2953 13.1283 11.4314 12.9993 12.585 13C13.7385 13 14.8746 13.1293 15.9933 13.388C17.1119 13.6467 18.213 14.034 19.2967 14.55C19.8036 14.8 20.2101 15.1627 20.5163 15.638C20.8225 16.1133 20.9753 16.634 20.9746 17.2V18C20.9746 18.55 20.7691 19.021 20.358 19.413C19.9469 19.805 19.4533 20.0007 18.8772 20ZM6.29273 18H18.8772V17.2C18.8772 17.0167 18.829 16.85 18.7325 16.7C18.636 16.55 18.5095 16.4333 18.3528 16.35C17.409 15.9 16.4564 15.5627 15.4951 15.338C14.5338 15.1133 13.5638 15.0007 12.585 15C11.6062 15 10.6361 15.1127 9.6748 15.338C8.71349 15.5633 7.76092 15.9007 6.81708 16.35C6.65977 16.4333 6.53288 16.55 6.4364 16.7C6.33992 16.85 6.29203 17.0167 6.29273 17.2V18ZM12.585 10C13.1618 10 13.6557 9.804 14.0668 9.412C14.4779 9.02 14.6831 8.54933 14.6824 8C14.6824 7.45 14.4768 6.979 14.0657 6.587C13.6546 6.195 13.1611 5.99933 12.585 6C12.0082 6 11.5142 6.196 11.1031 6.588C10.692 6.98 10.4869 7.45067 10.4876 8C10.4876 8.55 10.6931 9.021 11.1042 9.413C11.5153 9.805 12.0089 10.0007 12.585 10Z" fill="black"/>
    </svg> Max 8 orang

  </div>

  <div>
    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="24" viewBox="0 0 26 24" fill="none">
        <path d="M5.24378 19H4.19507L3.51341 17H2.09766V11.975C2.09766 11.425 2.3032 10.9583 2.7143 10.575C3.12539 10.1917 3.61898 10 4.19507 10V7C4.19507 6.45 4.40061 5.979 4.81171 5.587C5.2228 5.195 5.71639 4.99934 6.29248 5H18.877C19.4537 5 19.9477 5.196 20.3588 5.588C20.7699 5.98 20.9751 6.45067 20.9744 7V10C21.5512 10 22.0451 10.196 22.4562 10.588C22.8673 10.98 23.0725 11.4507 23.0718 12V17H21.656L20.9744 19H19.9257L19.244 17H5.92543L5.24378 19ZM13.6334 10H18.877V7H13.6334V10ZM6.29248 10H11.536V7H6.29248V10ZM4.19507 15H20.9744V12H4.19507V15Z" fill="black"/>
      </svg>4 kamar tidur
  </div>


  <div>
    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
        <g clip-path="url(#clip0_144_225)">
          <path d="M19.1063 10.9375H3.37567V3.90624C3.37491 3.64145 3.42924 3.37914 3.53551 3.13451C3.64178 2.88987 3.79788 2.66776 3.99478 2.48104L4.01116 2.46542C4.31974 2.17161 4.71592 1.9758 5.14557 1.90476C5.57523 1.83371 6.01742 1.89088 6.41172 2.06847C6.03933 2.65885 5.88455 3.35098 5.97197 4.03486C6.05939 4.71875 6.384 5.35516 6.8942 5.84296L7.34281 6.27073L6.51708 7.05815L7.44396 7.94198L8.26969 7.1546L12.845 2.79183L13.6708 2.00444L12.7439 1.12058L11.9181 1.90796L11.4695 1.48019C10.9324 0.969455 10.2244 0.654622 9.46881 0.590549C8.71327 0.526477 7.9581 0.717227 7.33491 1.12956C6.67775 0.733823 5.89899 0.56298 5.12654 0.645098C4.35409 0.727215 3.63391 1.05741 3.08429 1.58143L3.0679 1.59706C2.74888 1.8996 2.49596 2.25947 2.32378 2.65584C2.15159 3.05221 2.06356 3.47721 2.06479 3.90624V10.9375H0.753906V12.1875H2.06479V13.3867C2.06476 13.4875 2.0818 13.5876 2.11526 13.6832L3.33471 17.1714C3.39978 17.3582 3.52492 17.5206 3.69233 17.6357C3.85975 17.7508 4.06094 17.8127 4.26732 17.8125H4.79577L4.31787 19.375H5.68336L6.1613 17.8125H14.6861L15.1777 19.375H16.5459L16.0544 17.8125H16.9036C17.11 17.8127 17.3112 17.7509 17.4787 17.6358C17.6461 17.5207 17.7713 17.3582 17.8363 17.1714L19.0557 13.6832C19.0892 13.5876 19.1062 13.4875 19.1063 13.3867V12.1875H20.4171V10.9375H19.1063ZM7.8212 2.36405C8.18235 2.02043 8.6717 1.82744 9.18188 1.82744C9.69206 1.82744 10.1814 2.02043 10.5426 2.36405L10.9911 2.79183L8.26981 5.38671L7.8212 4.95901C7.46086 4.61463 7.25849 4.14801 7.25849 3.66153C7.25849 3.17505 7.46086 2.70843 7.8212 2.36405ZM17.7954 13.3359L16.6675 16.5625H4.50352L3.37567 13.3359V12.1875H17.7954V13.3359Z" fill="black"/>
        </g>
        <defs>
          <clipPath id="clip0_144_225">
            <rect width="20.9741" height="20" fill="white" transform="translate(0.0976562)"/>
          </clipPath>
        </defs>
      </svg>4 kamar Mandi dengan bathub dan shower

  </div>

  <div>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M20 15V3C15.184 7.594 14.977 11.319 15 15H20ZM20 15V21H19V18M8 12V18M4 3H12L11 12H5L4 3ZM7 18H9V21H7V18Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>1 Kitchen
  </div>

  <div>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M21 9V7C21 5.35 19.65 4 18 4H14C13.23 4 12.53 4.3 12 4.78C11.47 4.3 10.77 4 10 4H6C4.35 4 3 5.35 3 7V9C1.35 9 0 10.35 0 12V17C0 18.65 1.35 20 3 20V22H5V20H19V22H21V20C22.65 20 24 18.65 24 17V12C24 10.35 22.65 9 21 9ZM14 6H18C18.55 6 19 6.45 19 7V9.78C18.39 10.33 18 11.12 18 12V14H13V7C13 6.45 13.45 6 14 6ZM5 7C5 6.45 5.45 6 6 6H10C10.55 6 11 6.45 11 7V14H6V12C6 11.12 5.61 10.33 5 9.78V7ZM22 17C22 17.55 21.55 18 21 18H3C2.45 18 2 17.55 2 17V12C2 11.45 2.45 11 3 11C3.55 11 4 11.45 4 12V16H20V12C20 11.45 20.45 11 21 11C21.55 11 22 11.45 22 12V17Z" fill="black"/>
      </svg>1 Family Room
  </div>


<div>
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
  <g clip-path="url(#clip0_144_241)">
    <path d="M9 19V12.5M9 12.5V5.5H13.5C13.9596 5.5 14.4148 5.59053 14.8394 5.76642C15.264 5.94231 15.6499 6.20012 15.9749 6.52513C16.2999 6.85013 16.5577 7.23597 16.7336 7.66061C16.9095 8.08525 17 8.54037 17 9C17 9.45963 16.9095 9.91475 16.7336 10.3394C16.5577 10.764 16.2999 11.1499 15.9749 11.4749C15.6499 11.7999 15.264 12.0577 14.8394 12.2336C14.4148 12.4095 13.9596 12.5 13.5 12.5H9ZM12 23.5C5.649 23.5 0.5 18.351 0.5 12C0.5 5.649 5.649 0.5 12 0.5C18.351 0.5 23.5 5.649 23.5 12C23.5 18.351 18.351 23.5 12 23.5Z" stroke="black"/>
  </g>
  <defs>
    <clipPath id="clip0_144_241">
      <rect width="24" height="24" fill="white"/>
    </clipPath>
  </defs>
</svg>Parkir  Pribadi

</div>

<div>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path d="M6.35 15.35L4.25 13.2C5.28333 12.1667 6.46667 11.375 7.8 10.825C9.13333 10.275 10.5333 10 12 10C13.4667 10 14.871 10.2793 16.213 10.838C17.555 11.3967 18.734 12.2007 19.75 13.25L17.65 15.35C16.9 14.6 16.0373 14.0207 15.062 13.612C14.0867 13.2033 13.066 12.9993 12 13C10.9333 13 9.91233 13.2043 8.937 13.613C7.96167 14.0217 7.09933 14.6007 6.35 15.35ZM2.1 11.1L0 9C1.58333 7.38333 3.41267 6.14567 5.488 5.287C7.56333 4.42833 9.734 3.99933 12 4C14.2667 4 16.4377 4.42933 18.513 5.288C20.5883 6.14667 22.4173 7.384 24 9L21.9 11.1C20.5833 9.78333 19.0707 8.77067 17.362 8.062C15.6533 7.35333 13.866 6.99933 12 7C10.1333 7 8.34567 7.35433 6.637 8.063C4.92833 8.77167 3.416 9.784 2.1 11.1ZM12 21L8.475 17.45C8.94167 16.9833 9.47933 16.625 10.088 16.375C10.6967 16.125 11.334 16 12 16C12.6667 16 13.3043 16.125 13.913 16.375C14.5217 16.625 15.059 16.9833 15.525 17.45L12 21Z" fill="black"/>
      </svg>Wifi
</div>
<br>
 </div>
 <div class="col-3 d-flex align-items-stretch " style="margin: 20px 0px;">
    <div class="card border-dark mb-3">
      <div class="card-header" style="background-color: #57C5B6; color: white; text-align: center; font-weight: bold;">PRICE</div>
      <div class="card-body">
        <p class="card-text">
          <i class="fas fa-calendar" style="padding-right: 5px;"></i> Minggu, 22 April 2023
        </p>
        <p class="card-text">
          <i class="fas fa-calendar" style="padding-right: 5px;"></i> Minggu, 22 April 2023
        </p>
        <p class="card-text">
          <i class="fas fa-home" style="padding-right: 5px;"></i> Villa
        </p>
        <p class="text-center" style="font-weight: bold;">Arunika Villas</p>
        <p class="text-center"><del> From Rp 500.000</del></p>
        <p class="text-center" style="color: #FFD700;">Rp 450.000,00</p>
        <p class="text-center">Termasuk pajak & biaya layanan</p>
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
          <label class="form-check-label" for="flexCheckDefault">
              Saya setuju dengan syarat dan ketentuan
          </label>
        </div>
      </div>
      <div class="center">
        <button class="btn btn-utama" type="submit" style="background-color: #57C5B6; color: white;">Booking Sekarang</button>
      <br>
      <br>
      <br>
      </div>
    </div>
  </div>
</div>
</div>







@endsection
