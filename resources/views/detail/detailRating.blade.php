@extends('layouts.main')


@section('container')
<hr>


<!-- Menghubungkan dengan file CSS Bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css">
<!-- Menghubungkan dengan Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cabin&family=Nunito+Sans:wght@200&family=Nunito:ital,wght@0,200;0,400;0,500;1,200&family=Sorts+Mill+Goudy&display=swap" rel="stylesheet">
<div class="container">
<div class="row">

    <div class="photo-container">
        <img src="img/vila.png" alt="Photo 1">
        <img src="img/kamartidur1.png" alt="Photo 2">
        <img src="img/vila.png" alt="Photo 3">
    </div>
 </div>
 <div class="row">
    <div class ="col-9">


<br>
 <h1>Arunika Villas</h1>
    <div class="address">
        Jl. Naga, Kaliurang, Hargobinangun, Kec. Pakem, Kabupaten Sleman,Daerah Istimewa Yogyakarta 55582<br>
    </div>

</br>

<div class="btn-group" role="group" aria-label="Basic radio toggle button group">
  <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
  <label class="btn btn-outline-primary" for="btnradio1">deskripsi</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
  <label class="btn btn-outline-primary" for="btnradio2">fasilitas</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
  <label class="btn btn-outline-primary" for="btnradio3">Rating & Ulasan</label>
</div>

<br>


<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-qQX9sGhleOT61qr8w6H0P1C1D5n0eFxtHhTD2ugd3heIigp7pXE6a0u4sOCP7vucwb9GkD6nEV0RcMzryCCOg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .search-box {
            display: flex;
            align-items: center;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 15px;
            width: 30px;
        }
        .sort-box{
            display: flex;
            align-items: center;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 15px;
            width: 30px;

        }

    </style>
</head>
<body>
    <div class="search-box">
        <i class="fas fa-search" style="margin-right: 5px;"></i>
    </div>
    <div class="sort-box ">
        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12" viewBox="0 0 18 12" fill="none">
        <path d="M0 12V10H6V12H0ZM0 7V5H12V7H0ZM0 2V0H18V2H0Z" fill="black"/>
      </svg>
    </div>





</body>










<br>
 </div>
 <div class="col-3 d-flex align-items-stretch " style="margin: 20px 0px;">
    <div class="card border-dark mb-3">
      <div class="card-header" style="background-color: #57C5B6; color: white; text-align: center; font-weight: bold;">PRICE</div>
      <div class="card-body">
        <p class="card-text">
          <i class="fas fa-calendar" style="padding-right: 5px;"></i> Minggu, 22 April 2023
        </p>
        <p class="card-text">
          <i class="fas fa-calendar" style="padding-right: 5px;"></i> Minggu, 22 April 2023
        </p>
        <p class="card-text">
          <i class="fas fa-home" style="padding-right: 5px;"></i> Villa
        </p>
        <p class="text-center" style="font-weight: bold;">Arunika Villas</p>
        <p class="text-center"><del> From Rp 500.000</del></p>
        <p class="text-center" style="color: #FFD700;">Rp 450.000,00</p>
        <p class="text-center">Termasuk pajak & biaya layanan</p>
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
          <label class="form-check-label" for="flexCheckDefault">
              Saya setuju dengan syarat dan ketentuan
          </label>
        </div>
      </div>
      <div class="center">
        <button class="btn btn-utama" type="submit" style="background-color: #57C5B6; color: white;">Booking Sekarang</button>
      <br>
      <br>
      <br>
      </div>
    </div>
  </div>
</div>
</div>







@endsection
