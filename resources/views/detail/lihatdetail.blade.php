@extends('layouts.main')


@section('container')
<hr>


<!-- Menghubungkan dengan file CSS Bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css">
<!-- Menghubungkan dengan Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cabin&family=Nunito+Sans:wght@200&family=Nunito:ital,wght@0,200;0,400;0,500;1,200&family=Sorts+Mill+Goudy&display=swap" rel="stylesheet">


<div class="container">
  <div class="row">
    @for ($i = 0; $i < 3; $i++)
    <div class="col-auto form-border">
      <div class="col-md-200">
          <div class="row g-100">
            <div class="col-md-4 d-flex align-items-center justify-content-center">
            <img src="img/vila.png" class="img-fluid  rounded " alt="Foto 1">
            </div>
            <div class="col-md-8">
              <div class="card-body">
              <h5 class="card-title" style="font-family: 'Sorts Mill Goudy', serif;">ARUNIKA VILLA</h5>
              <p style="font-family: 'Sorts Mill Goudy', serif;">
                <i class="bi bi-person me-2"></i>
                <span class="small">max 10 orang</span>
                </p>
                <p style="font-family: 'Sorts Mill Goudy', serif;">
                <i class="fas fa-bed me-2"></i>
                <span class="small">5 kamar tidur</span>
                </p>
                <p style="font-family: 'Sorts Mill Goudy', serif;">
                <i class="fas fa-shower me-2"></i>
                <span class="small">5 kamar mandi</span> 
                </p>

     <div class="d-flex justify-content-end align-items-start">
  <div class="text-end me-3" style="text-align: right;">
    <p class="small text-decoration-line-through" style="font-family: 'Sorts Mill Goudy', serif;">Rp 550.000,00</p>
    <p class="h5 fw-bold text-warning" style="font-family: 'Sorts Mill Goudy', serif;">Rp 450.000,00</p>
  </div>
  <div class="text-end">
    <div class="rating">
      <span class="star"><i class="bi bi-star-fill text-warning"></i></span>
      <span class="star"><i class="bi bi-star-fill text-warning"></i></span>
      <span class="star"><i class="bi bi-star-fill text-warning"></i></span>
      <span class="star"><i class="bi bi-star-fill text-warning"></i></span>
      <span class="star"><i class="bi bi-star-fill text-warning"></i></span>
    </div>
    <p class="small">5.0 dari 5 ulasan</p>
  </div>
</div>

  </div>
  </div>
  </div>
        
  </div>
    </div>
    
    @endfor
  </div>
</div>


<!-- Menghubungkan dengan file JavaScript Bootstrap -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.min.js"></script>

<hr>
@endsection