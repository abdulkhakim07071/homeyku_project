@extends('layouts.main')


@section('container')
<hr>


<!-- Menghubungkan dengan file CSS Bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css">
<!-- Menghubungkan dengan Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Cabin&family=Nunito+Sans:wght@200&family=Nunito:ital,wght@0,200;0,400;0,500;1,200&family=Sorts+Mill+Goudy&display=swap" rel="stylesheet">
<div class="container">
<div class="row">

    <div class="photo-container">
    </div>
 </div>
 <div class="row">
    <div class ="col-9">


<br>
 <h1>{{ $penginapan->nama_penginapan }}</h1>
    <div class="address">
        {{ $penginapan->alamat }}<br>
    </div>

</br>

<div class="btn-group" role="group" aria-label="Basic radio toggle button group">
  <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
  <label class="btn btn-outline-primary" for="btnradio1">deskripsi</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
  <label class="btn btn-outline-primary" for="btnradio2">fasilitas</label>

  <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
  <label class="btn btn-outline-primary" for="btnradio3">Rating & Ulasan</label>
</div>

<br>
<div class="address">
{{$penginapan->deskripsi}}     </div>
 </div>
 <div class="col-3 d-flex align-items-stretch " style="margin: 20px 0px;">
    <div class="card border-dark mb-3">
    <form id="bookingForm" action="{{ route('bookingvilla') }}" enctype="multipart/form-data">
      <div class="card-header" style="background-color: #57C5B6; color: white; text-align: center; font-weight: bold;">PRICE</div>
      <div class="card-body">
        <p class="card-text">
          <i class="fas fa-calendar" style="padding-right: 5px;">  Check in</i><input type="date" class="form-control" id="checkin" name="checkin" value="{{ now()->format('Y-m-d') }}" min="{{ now()->format('Y-m-d') }}">

        </p>
        <p class="card-text">
          <i class="fas fa-calendar" style="padding-right: 5px;">  Check Out</i>         <input type="date" class="form-control" id="checkout" name="checkout" value="{{ date('Y-m-d', strtotime('+1 day', strtotime(now()->format('Y-m-d')))) }}" min="{{ date('Y-m-d', strtotime('+1 day', strtotime(now()->format('Y-m-d')))) }}">

        </p>
        <p class="card-text">
          <i class="fas fa-home" style="padding-right: 5px;"></i> Villa
        </p>
        <p class="text-center" style="font-weight: bold;">{{ $penginapan->nama_penginapan }}</p>
        <p class="text-center"><del>Rp. {{$penginapan->harga+($penginapan->harga/100*10)}},00/malam</del></p>
        <p class="text-center" style="color: #FFD700;">Rp. {{$penginapan->harga}},00/malam</p>
        <p class="text-center"></p>
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="false" id="agreeCheckbox">
          <label class="form-check-label" for="agreeCheckbox">
              Saya setuju dengan syarat dan ketentuan
          </label>
        </div>
      </div>
      <div class="center">
        <a>
          <input type="hidden" value="{{ $penginapan->id }}" id="id" name="id">
        <button class="btn btn-utama" type="submit" style="background-color: #57C5B6; color: white;" id="bookingButton" disabled>Booking Sekarang</button></a>

      <br>
</form>
      <br>
      <br>
      </div>
    </div>
  </div>
</div>
</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
    const agreeCheckbox = document.getElementById('agreeCheckbox');
    const bookingButton = document.getElementById('bookingButton');
    const bookingForm = document.getElementById('bookingForm');

    agreeCheckbox.addEventListener('change', function() {
        if (this.checked) {
            bookingButton.removeAttribute('disabled'); // Menghapus atribut 'disabled'
        } else {
            bookingButton.setAttribute('disabled', 'true'); // Menambah atribut 'disabled'
        }
    });

    bookingForm.addEventListener('submit', function(event) {
        if (!agreeCheckbox.checked) {
            event.preventDefault();
            alert('Anda harus menyetujui ketentuan booking sebelum melakukan submit.');
        }
    });
});

</script>





@endsection
